            <!--
            <div class="b-features">
                    <div class="container">
                            <div class="row">
                                    <div class="col-md-9 col-md-offset-3 col-xs-6 col-xs-offset-6">
                                            <ul class="b-features__items">
                                                    <li class="wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">Low Prices, No Haggling</li>
                                                    <li class="wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">Largest Car Dealership</li>
                                                    <li class="wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">Multipoint Safety Check</li>
                                            </ul>
                                    </div>
                            </div>
                    </div>
            </div> 
            b-features-->
            <div class="b-info" style="padding: 25px 0 25px 0;">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-xs-6">
						<aside class="b-info__aside wow zoomInLeft" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">
							<article class="b-info__aside-article">
								<h3>Acara Rutin</h3>
								<div class="b-info__aside-article-item">
									<h6>Kopi Darat</h6>
									<p>Pukul 21:00 - 02:00<br>
										Setiap Hari Sabtu</p>
								</div>
								<div class="b-info__aside-article-item">
									<h6>Bakti Sosial</h6>
									<p>Tanggal 1 - 10<br>
										Setiap Awal Bulan</p>
								</div>
							</article>
							<article class="b-info__aside-article">
								<h3>Tentang Kami</h3>
								<p>Secara resmi dideklarasikan di Surabaya pada tanggal 18 Februari 2017 dan hingga saat ini telah memiliki lebih dari 1000 member yg tersebar di seluruh nusantara</p>
							</article>
							<a href="<?= base_url() ?>about" class="btn m-btn">Read More<span class="fa fa-angle-right"></span></a>
						</aside>
					</div>
					<div class="col-md-3 col-xs-6">
						<div class="b-info__latest">
							<h3>ACARA TAHUN INI</h3>
							<div class="b-info__latest-article wow zoomInUp" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">
								<div class="b-info__latest-article-photo m-audi"></div>
								<div class="b-info__latest-article-info">
									<h6><a href="<?= base_url() ?>detail">Goes To Bali</a></h6>
									<p><span class="fa fa-tachometer"></span> 9,000 KM</p>
								</div>
							</div>
							<div class="b-info__latest-article wow zoomInUp" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">
								<div class="b-info__latest-article-photo m-audiSpyder"></div>
								<div class="b-info__latest-article-info">
									<h6><a href="<?= base_url() ?>shop">Anniversary</a></h6>
									<p><span class="fa fa-tachometer"></span> 12,000 KM</p>
								</div>
							</div>
							<div class="b-info__latest-article wow zoomInUp" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">
								<div class="b-info__latest-article-photo m-aston"></div>
								<div class="b-info__latest-article-info">
									<h6><a href="<?= base_url() ?>shop">National</a></h6>
									<p><span class="fa fa-tachometer"></span> 18,000 KM</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-xs-6">
						<div class="b-info__twitter">
							<h3>Sosial Media</h3>
							<div class="b-info__twitter-article wow zoomInUp" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">
								<div class="b-info__twitter-article-icon"><span class="fa fa-comments"></span></div>
								<div class="b-info__twitter-article-content">
									<p>Om pada ngumpul dimana nih?</p>
									<span>17 minutes ago</span>
								</div>
							</div>
							<div class="b-info__twitter-article wow zoomInUp" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">
								<div class="b-info__twitter-article-icon"><span class="fa fa-comments"></span></div>
								<div class="b-info__twitter-article-content">
									<p>Event kemarin gilaaaa .. seru banget ..</p>
									<span>31 menit yang lalu</span>
								</div>
							</div>
							<div class="b-info__twitter-article wow zoomInUp" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">
								<div class="b-info__twitter-article-icon"><span class="fa fa-comments"></span></div>
								<div class="b-info__twitter-article-content">
									<p>Are you ready to have fun guys?</p>
									<span>38 menit yang lalu</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-xs-6">
						<address class="b-info__contacts wow zoomInUp" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">
							<p>kontak kami</p>
							<div class="b-info__contacts-item">
								<span class="fa fa-map-marker"></span>
								<em>Surabaya</em>
							</div>
							<div class="b-info__contacts-item">
								<span class="fa fa-phone"></span>
								<em>Phone:  081-232-646-xxx</em>
							</div>
							<div class="b-info__contacts-item">
								<span class="fa fa-fax"></span>
								<em>FAX:  1-800-624-5462</em>
							</div>
							<div class="b-info__contacts-item">
								<span class="fa fa-envelope"></span>
								<em>Email:  support@pib-brawijaya.com</em>
							</div>
						</address>
						<address class="b-info__map">
							<a href="<?= base_url() ?>contact">Open Location Map</a>
						</address>
					</div>
				</div>
			</div>
		</div><!--b-info-->

                <footer class="b-footer">
			<a id="to-top" href="<?= base_url() ?>#this-is-top" style="display: none;"><i class="fa fa-chevron-up"></i></a>
			<div class="container">
				<div class="row">
					<div class="col-xs-4">
                                            <?php /*
                                                <div class="b-footer__company wow slideInLeft" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">
							<div class="b-nav__logo">
								<h3><a href="<?= base_url() ?>">Brawijaya<span>Chapter</span></a></h3>
							</div>
							<p>© 2018 Powered by iJarvis.</p>
						</div>
                                            */ ?>
                                                <div class="wow slideInLeft" data-wow-delay="0.3s" style="margin-top: -20px;visibility: visible; animation-delay: 0.3s; animation-name: slideInLeft;">
                                                    <h2>
                                                        <a href="<?= base_url() ?>">
                                                            <img src="<?= base_url() ?>assets/images/logo/logo-image.png"></img>
                                                            <img src="<?= base_url() ?>assets/images/logo/logo-text.png"></img>
                                                        </a>
                                                    </h2>
                                                    <p>© 2018 Powered by iJarvis.</p>
						</div>
					</div>
					<div class="col-xs-8">
						<div class="b-footer__content wow slideInRight" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">
							<div class="b-footer__content-social">
								<a href="#" target="_blank"><span class="fa fa-facebook-square"></span></a>
								<a href="#" target="_blank"><span class="fa fa-twitter-square"></span></a>
								<a href="#" target="_blank"><span class="fa fa-google-plus-square"></span></a>
								<a href="#" target="_blank"><span class="fa fa-instagram"></span></a>
								<a href="#" target="_blank"><span class="fa fa-youtube-square"></span></a>
								<a href="#" target="_blank"><span class="fa fa-skype"></span></a>
							</div>
							<nav class="b-footer__content-nav">
								<ul>
									<li><a href="<?= base_url() ?>">Home</a></li>
									<li><a href="<?= base_url() ?>event/touring">Kegiatan</a></li>
									<li><a href="<?= base_url() ?>community/member">Komunitas</a></li>
									<li><a href="<?= base_url() ?>about">Tentang Kami</a></li>
									<li><a href="<?= base_url() ?>contact">Kontak</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</footer><!--b-footer-->
		<!--Main-->   
		<script src="<?= base_url() ?>assets/js/modernizr.custom.js"></script>

		<script src="<?= base_url() ?>assets/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
		<script src="<?= base_url() ?>assets/js/waypoints.min.js"></script>
		<script src="<?= base_url() ?>assets/js/jquery.easypiechart.min(1).js"></script>
		<script src="<?= base_url() ?>assets/js/classie.js"></script>

		<!--Switcher-->
		<script src="<?= base_url() ?>assets/switcher/js/switcher.js"></script>
		<!--Owl Carousel-->
		<script src="<?= base_url() ?>assets/owl-carousel/owl.carousel.min.js"></script>
		<!--bxSlider-->
		<script src="<?= base_url() ?>assets/bxslider/jquery.bxslider.js"></script>
		<!-- jQuery UI Slider -->
		<script src="<?= base_url() ?>assets/js/jquery.ui-slider.js"></script>
                
		<!--Theme-->
		<script src="<?= base_url() ?>assets/js/jquery.smooth-scroll.js"></script>
		<script src="<?= base_url() ?>assets/js/wow.min.js"></script>
		<script src="<?= base_url() ?>assets/js/jquery.placeholder.min.js"></script>
		<script src="<?= base_url() ?>assets/js/theme.js"></script>
	
</body></html>