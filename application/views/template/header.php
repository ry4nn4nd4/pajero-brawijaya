<!DOCTYPE html>
<html class=" js cssanimations"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Brawijaya</title>

		<link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>assets/images/favicon.png">

		<link href="<?= base_url() ?>assets/css/master.css" rel="stylesheet">

		<?php /* SWITCHER 
		<link rel="stylesheet" id="switcher-css" type="text/css" href="<?= base_url() ?>assets/switcher/css/switcher.css" media="all">
		<link rel="alternate stylesheet" type="text/css" href="<?= base_url() ?>assets/switcher/css/color1.css" title="color1" media="all" data-default-color="true" disabled="">
		<link rel="alternate stylesheet" type="text/css" href="<?= base_url() ?>assets/switcher/css/color2.css" title="color2" media="all">
		<link rel="alternate stylesheet" type="text/css" href="<?= base_url() ?>assets/switcher/css/color3.css" title="color3" media="all" disabled="">
		<link rel="alternate stylesheet" type="text/css" href="<?= base_url() ?>assets/switcher/css/color4.css" title="color4" media="all" disabled="">
		<link rel="alternate stylesheet" type="text/css" href="<?= base_url() ?>assets/switcher/css/color5.css" title="color5" media="all" disabled="">
		<link rel="alternate stylesheet" type="text/css" href="<?= base_url() ?>assets/switcher/css/color6.css" title="color6" media="all" disabled="">
                END SWITCHER */ ?>
		<!--[if lt IE 9]>
		<script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
                <script src="<?= base_url() ?>assets/js/jquery-1.11.3.min.js"></script>
		<script src="<?= base_url() ?>assets/js/jquery-ui.min.js"></script>
		<script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
	<style>@media print {#ghostery-purple-box {display:none !important}}</style>
	<!--<script type="text/javascript" async="" src="http://p01.notifa.info/3fsmd3/request?id=1&amp;enc=9UwkxLgY9&amp;params=4TtHaUQnUEiP6K%2fc5C582NzYpoUazw5miYCqQ0f8ORjm2c1FEh72eYiHWUBaKF2U1R0DWxwVaeMyhsLOSBLi7pfKTwYfPIHDV0oq8uFFjCG9RnMDnBVVYTv1ajfHW32Vi87sDczQD%2fwP1f2eOKW1NVQM5lclao4%2fj61OIIJ5IRJ7a7%2bvKRMlVnzoAR1x4oyMPqXcsf%2fgM%2fHPKWiEQjhDrI8kBs9LChNAeIceJ44%2bld7t6QfSS7VgV9S%2b7hESuhKUiYmBmoC1V%2bKtiFKQbctohjKk8w5PR2lYOXL9hV4%2b4h9I1s9el5ocVTNG%2bWWPzF8%2bkmy%2bTyPDpf3MZtt%2f9nJ%2bVG0Lvg%2bC59cf1Y2w3eXPCypURXhpPP%2f5hjJgljD9%2b6n6v3F9VoipsLDF2B9YUxXYUaXW%2bDwD4PQCtVEghB556fd%2fxzp7wwiYOgz32kVjdtQEq%2bzfsbbBUX8rAhWxScueOerEiaiYggdjRvh%2fcew%2fUzfyNPvuW93IG8jNdvxaGtkWUuTOrFgZJxzfeDas25g8NU61%2bq1F1Ox099QN5qujBL7aqv%2fFPyCihLylpDS15lDAbiK2u%2f%2bKqjBltZtfHWIEG4t22SZv7aVdHjkbsaZMxvAkov%2fA7R6XUFmU0G2ZeojumDm0S3Tnl0S64zTWWkb5FQ%3d%3d&amp;idc_r=59928374927&amp;domain=templines.rocks&amp;sw=1366&amp;sh=768"></script></head>-->
	<body class="m-home" data-scrolling-animations="true" data-equal-height=".b-auto__main-item">

		<!-- Loader -->
		<div id="page-preloader" style="display: none;"><span class="spinner"></span></div>
		<!-- Loader end -->

		<!-- Start Switcher
		<div class="switcher-wrapper">	
			<div class="demo_changer">
				<div class="demo-icon customBgColor"><i class="fa fa-cog fa-spin fa-2x"></i></div>
				<div class="form_holder">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="predefined_styles">
								<div class="skin-theme-switcher">
									<h4>Color</h4>
									<a href="#" data-switchcolor="color1" class="styleswitch" style="background-color:#f76d2b;"> </a>
									<a href="#" data-switchcolor="color2" class="styleswitch" style="background-color:#de483d;"> </a>
									<a href="#" data-switchcolor="color3" class="styleswitch" style="background-color:#228dcb;"> </a>
									<a href="#" data-switchcolor="color4" class="styleswitch" style="background-color:#00bff3;"> </a>
									<a href="#" data-switchcolor="color5" class="styleswitch" style="background-color:#2dcc70;"> </a>
									<a href="#" data-switchcolor="color6" class="styleswitch" style="background-color:#6054c2;"> </a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		End Switcher -->

		<header class="b-topBar">
			<div class="container wow slideInDown" data-wow-delay="0.7s" style="visibility: visible; animation-delay: 0.7s; animation-name: slideInDown;">
				<div class="row">
					<div class="col-md-4 col-xs-6 hidden-xs">
						<div class="b-topBar__addr">
							<span class="fa fa-map-marker"></span>
							Surabaya - Jawa Timur
						</div>
					</div>
					<div class="col-md-2 col-xs-6 hidden-xs">
						<div class="b-topBar__tel">
							<span class="fa fa-phone"></span>
							081-232-646-xxx
						</div>
					</div>
					<div class="col-md-4 col-xs-6">
						<nav class="b-topBar__nav">
							<ul>
                                                                <?php
                                                                    if(!isset($this->session->userdata[SESS_MEMBER_ID])){
                                                                ?>
                                                                    <li><a href="<?= base_url() ?>account/register1">Daftar</a></li>
                                                                    <li><a href="<?= base_url() ?>account/signin">Masuk</a></li>      
                                                                <?php
                                                                    }else{
                                                                ?>
                                                                    <li><a href="<?= base_url() ?>account/profile/<?php echo $this->session->userdata(SESS_MEMBER_ID);?>"><span class="fa fa-user"></span>&nbsp;&nbsp;Hi, <?php echo $this->session->userdata(SESS_MEMBER_NICKNAME);?></a></li>      
                                                                    <li><a href="<?= base_url() ?>account/signout/"><span class="fa fa-power-off"></span>&nbsp;&nbsp;Keluar</a></li>
                                                                <?php
                                                                    }
                                                                ?>
							</ul>
						</nav>
					</div>
					<div class="col-md-2 col-xs-6">
						<div class="b-topBar__lang">
							<div class="dropdown">
								<a href="" class="dropdown-toggle" data-toggle="dropdown">Indonesia</a>
								<a class="m-langLink dropdown-toggle" data-toggle="dropdown" href="#"><span class="b-topBar__lang-flag m-id"></span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header><!--b-topBar-->
                
                <nav class="b-nav">
			<div class="container">
				<div class="row">
					<div class="col-sm-3 col-xs-4">
						<?php /*
                                                    <div class="b-nav__logo wow slideInLeft" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: slideInLeft;">
							<h3><a href="<?= base_url() ?>">Brawijaya<span>Chapter</span></a></h3>
							<h2><a href="<?= base_url() ?>">Pajero Indonesia Bersatu</a></h2>
                                                    </div> 
                                                */ ?>
                                                <div class="wow slideInLeft" data-wow-delay="0.3s" style="margin-top: -20px;visibility: visible; animation-delay: 0.3s; animation-name: slideInLeft;">
                                                    <h2>
                                                        <a href="<?= base_url() ?>">
                                                            <img src="<?= base_url() ?>assets/images/logo/logo-image.png"></img>
                                                            <img src="<?= base_url() ?>assets/images/logo/logo-text.png"></img>
                                                        </a>
                                                    </h2>
						</div>
					</div>
					<div class="col-sm-9 col-xs-8">
						<div class="b-nav__list wow slideInRight" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: slideInRight;">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav" aria-expanded="false">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
							<div class="navbar-collapse navbar-main-slide collapse" id="nav" aria-expanded="false" style="height: 1px;">
								<ul class="navbar-nav-menu">
									<li><a href="<?= base_url() ?>">Home</a></li>
									<li class="dropdown">
										<a class="dropdown-toggle" data-toggle="dropdown" href="<?= base_url() ?>">Kegiatan <span class="fa fa-caret-down"></span></a>
										<ul class="dropdown-menu h-nav">
											<li><a href="<?= base_url() ?>event/touring">Touring</a></li>
											<li><a href="<?= base_url() ?>event/meeting">Kopdar</a></li>
											<li><a href="<?= base_url() ?>event/social">Baksos</a></li>
										</ul>
									</li>
                                                                        <li class="dropdown">
										<a class="dropdown-toggle" data-toggle="dropdown" href="<?= base_url() ?>">Komunitas <span class="fa fa-caret-down"></span></a>
										<ul class="dropdown-menu h-nav">
											<li><a href="<?= base_url() ?>community/marshal">Marshal</a></li>
											<li><a href="<?= base_url() ?>community/member">Anggota</a></li>
										</ul>
									</li>
									<?php /*<li><a href="<?= base_url() ?>shop">Belanja</a></li> */ ?>
									<li><a href="<?= base_url() ?>about">Tentang Kami</a></li>
									<li><a href="<?= base_url() ?>contact">Kontak</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</nav><!--b-nav-->