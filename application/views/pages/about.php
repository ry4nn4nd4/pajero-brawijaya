<section class="b-pageHeader">
    <div class="container">
            <h1 class="wow zoomInLeft" data-wow-delay="0.7s" style="visibility: visible; animation-delay: 0.7s; animation-name: zoomInLeft;">Tentang Kami</h1>
            <div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0.7s" style="visibility: visible; animation-delay: 0.7s; animation-name: zoomInRight;">
                    <h3>Kenal kami lebih jauh</h3>
            </div>
    </div>
</section><!--b-pageHeader-->

<div class="b-breadCumbs s-shadow">
        <div class="container">
                <a href="<?= base_url() ?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span>
                <a href="<?= base_url() ?>/about" class="b-breadCumbs__page m-active">Tentang Kami</a>
        </div>
</div><!--b-breadCumbs-->

<section class="b-best">
        <div class="container">
                <div class="row">
                        <div class="col-sm-6 col-xs-12">
                                <div class="b-best__info">
                                        <header class="s-lineDownLeft b-best__info-head">
                                                <h2 class="wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">Pajero Indonesia Bersatu</h2>
                                        </header>
                                        <h6 class="wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">Secara resmi dideklarasikan di Surabaya pada tanggal 18 Februari 2017 dan hingga saat ini telah memiliki lebih dari 1000 member yg tersebar di seluruh nusantara</h6>
                                        <p class="wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">PIB didirikan dengan semangat kekeluargaan dan menjunjung tinggi toleransi serta kepedulian antar member maupun dengan masyarakat. Dan juga telah diakui secara legal oleh Kemenkumham yg pendiriannya tertuang dalam Akta Notaris serta memiliki AD/ART dalam menjalankan roda organisasi</p>
                                        <?php /*
                                        <a href="<?= base_url() ?>community" class="btn m-btn m-readMore wow zoomInUp" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">view listings<span class="fa fa-angle-right"></span></a>
                                         * */ ?>
                                </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                                <img class="img-responsive center-block wow zoomInUp" data-wow-delay="0.5s" alt="best" src="<?= base_url() ?>assets/images/member/default_marshal.png" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                        </div>
                </div>
        </div>
</section><!--b-best-->

<section class="b-what s-shadow m-home">
        <div class="container">
                <h3 class="s-titleBg wow zoomInUp" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">Pendekar Brawijaya</h3><br>
                <h2 class="s-title wow zoomInUp" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">Marshal</h2>
                <div class="row">
                        <div class="col-sm-4 col-xs-12">
                                <div class="b-world__item wow zoomInLeft" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">
                                        <img class="img-responsive" src="<?= base_url() ?>assets/images/member/2.jpg" alt="Ketua">
                                        <div class="b-world__item-val">
                                                <span class="b-world__item-val-title">Suryadi</span>
                                        </div>
                                        <h2>Ketua Chapter Brawijaya</h2>
                                        <p>Jangan nakal ya ...</p>
                                </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                                <div class="b-world__item wow zoomInUp" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">
                                        <img class="img-responsive" src="<?= base_url() ?>assets/images/member/3.jpg" alt="Sekretaris">
                                        <div class="b-world__item-val">
                                                <span class="b-world__item-val-title">Komang</span>
                                        </div>
                                        <h2>Sekretaris Chapter Brawijaya</h2>
                                        <p>Om Swastyastu ... </p>
                                </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                                <div class="b-world__item wow zoomInRight" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">
                                        <img class="img-responsive" src="<?= base_url() ?>assets/images/member/default_member.png" alt="Bendahara">
                                        <div class="b-world__item-val">
                                                <span class="b-world__item-val-title">Oscar</span>
                                        </div>
                                        <h2>Warrior Chapter Brawijaya</h2>
                                        <p>Ayooo ngopi om ...</p>
                                </div>
                        </div>
                </div>
        </div>
</section><!--b-what-->

<section class="b-more">
        <div class="container">
                <div class="row">
                        <div class="col-sm-6 col-xs-12">
                                <div class="b-more__why wow zoomInLeft" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">
                                        <h2 class="s-title">Bergabunglah dengan kami</h2>
                                        <p>Curabitur libero. Donec facilisis velit eu est. Phasellus cons quat. Aenean vitae quam. Vivamus etyd nunc. Nunc consequsem velde metus imperdiet lacinia. Lorem ipsum dolor sit amet sed consectetur adipisicing elit sed do eiusmod.</p>
                                        <ul class="s-list">
                                                <li><span class="fa fa-check"></span>Donec facilisis velit eu est phasellus consequat quis nostrud</li>
                                                <li><span class="fa fa-check"></span>Aenean vitae quam. Vivamus et nunc nunc conseq</li>
                                                <li><span class="fa fa-check"></span>Sem vel metus imperdiet lacinia enean </li>
                                                <li><span class="fa fa-check"></span>Dapibus aliquam augue fusce eleifend quisque tels</li>
                                                <li><span class="fa fa-check"></span>Lorem ipsum dolor sit amet, consectetur </li>
                                                <li><span class="fa fa-check"></span>Adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore Magna </li>
                                        </ul>
                                </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                                <div class="b-more__info wow zoomInRight" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">
                                        <h2 class="s-title">BUDAYA KAMI</h2>
                                        <div class="b-more__info-block">
                                                <div class="b-more__info-block-title">SOLIDARITAS<a class="j-more" href="#"><span class="fa fa-angle-left"></span></a></div>
                                                <div class="b-more__info-block-inside j-inside">
                                                        <p>Perbedaan takkan memisahkan kita, perbedaan yang menyatukan kita</p>
                                                </div>
                                        </div>
                                        <div class="b-more__info-block">
                                                <div class="b-more__info-block-title">SANTUN<a href="#" class="j-more"><span class="fa fa-angle-left"></span></a></div>
                                                <div class="b-more__info-block-inside j-inside">
                                                        <p>Diantara banyak kehidupan, etika-lah yang kami utamakan</p>
                                                </div>
                                        </div>
                                        <div class="b-more__info-block">
                                                <div class="b-more__info-block-title">KREATIF<a href="#" class="j-more"><span class="fa fa-angle-left"></span></a></div>
                                                <div class="b-more__info-block-inside j-inside">
                                                        <p>Gali potensi dirimu tanpa batas</p>
                                                </div>
                                        </div>
                                        <div class="b-more__info-block">
                                                <div class="b-more__info-block-title">WIBAWA<a href="#" class="j-more"><span class="fa fa-angle-left"></span></a></div>
                                                <div class="b-more__info-block-inside j-inside">
                                                        <p>Saat tua & muda bersatu, tidak ada satupun yang dapat mengusik</p>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</section><!--b-more-->

<section class="b-personal s-shadow">
        <div class="container">
                <h3 class="s-titleBg wow zoomInUp" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">the people you can trust ON</h3><br>
                <h2 class="s-title wow zoomInUp" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">OUR TEAM MEMBERS</h2>
                <div id="carousel-small" class="owl-carousel enable-owl-carousel owl-theme" data-items="4" data-navigation="true" data-auto-play="true" data-stop-on-hover="true" data-items-desktop="4" data-items-desktop-small="3" data-items-tablet="2" data-items-tablet-small="2" style="opacity: 1; display: block;">
                        <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 4560px; left: 0px; display: block; transition: all 800ms ease; transform: translate3d(-570px, 0px, 0px);"><div class="owl-item" style="width: 285px;"><div>
                                <div class="b-personal__worker wow zoomInUp" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">
                                        <div class="b-personal__worker-img">
                                                <img src="<?= base_url() ?>assets/images/about/jason.jpg" class="img-responsive" alt="jason">
                                                <div class="b-personal__worker-img-social">
                                                        <div class="b-personal__worker-img-social-main">
                                                                <a href="#"><span class="fa fa-facebook-square"></span></a>
                                                                <a href="#"><span class="fa fa-twitter-square"></span></a>
                                                                <a href="#"><span class="fa fa-pinterest-square"></span></a>
                                                                <a href="#"><span class="fa fa-google-plus-square"></span></a>
                                                        </div>
                                                </div>
                                        </div>
                                        <h6>Owner / Co-Invester</h6>
                                        <div class="b-personal__worker-name s-lineDownLeft">
                                                <h4 class="s-titleDet">Jason Roy</h4>
                                        </div>
                                        <p>Owner of AutoClub, started the company
                                                in 2005. A very hard working person who loves to drive the cars.</p>
                                </div>
                        </div></div><div class="owl-item" style="width: 285px;"><div>
                                <div class="b-personal__worker wow zoomInUp" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">
                                        <div class="b-personal__worker-img">
                                                <img src="<?= base_url() ?>assets/images/about/paul.jpg" class="img-responsive" alt="paul">
                                                <div class="b-personal__worker-img-social">
                                                        <div class="b-personal__worker-img-social-main">
                                                                <a href="#"><span class="fa fa-facebook-square"></span></a>
                                                                <a href="#"><span class="fa fa-twitter-square"></span></a>
                                                                <a href="#"><span class="fa fa-pinterest-square"></span></a>
                                                                <a href="#"><span class="fa fa-google-plus-square"></span></a>
                                                        </div>
                                                </div>
                                        </div>
                                        <h6>Delivery Incharge</h6>
                                        <div class="b-personal__worker-name s-lineDownLeft">
                                                <h4 class="s-titleDet">Paul Richard</h4>
                                        </div>
                                        <p>Owner of AutoClub, started the company
                                                in 2005. A very hard working person who loves to drive the cars.</p>
                                </div>
                        </div></div><div class="owl-item" style="width: 285px;"><div>
                                <div class="b-personal__worker wow zoomInUp" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">
                                        <div class="b-personal__worker-img">
                                                <img src="<?= base_url() ?>assets/images/about/peter.jpg" class="img-responsive" alt="peter">
                                                <div class="b-personal__worker-img-social">
                                                        <div class="b-personal__worker-img-social-main">
                                                                <a href="#"><span class="fa fa-facebook-square"></span></a>
                                                                <a href="#"><span class="fa fa-twitter-square"></span></a>
                                                                <a href="#"><span class="fa fa-pinterest-square"></span></a>
                                                                <a href="#"><span class="fa fa-google-plus-square"></span></a>
                                                        </div>
                                                </div>
                                        </div>
                                        <h6>Auto Reviewer / Technican</h6>
                                        <div class="b-personal__worker-name s-lineDownLeft">
                                                <h4 class="s-titleDet">Peter Harold</h4>
                                        </div>
                                        <p>Owner of AutoClub, started the company
                                                in 2005. A very hard working person who loves to drive the cars.</p>
                                </div>
                        </div></div><div class="owl-item" style="width: 285px;"><div>
                                <div class="b-personal__worker wow zoomInUp" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">
                                        <div class="b-personal__worker-img">
                                                <img src="<?= base_url() ?>assets/images/about/maria.jpg" class="img-responsive" alt="maria">
                                                <div class="b-personal__worker-img-social">
                                                        <div class="b-personal__worker-img-social-main">
                                                                <a href="#"><span class="fa fa-facebook-square"></span></a>
                                                                <a href="#"><span class="fa fa-twitter-square"></span></a>
                                                                <a href="#"><span class="fa fa-pinterest-square"></span></a>
                                                                <a href="#"><span class="fa fa-google-plus-square"></span></a>
                                                        </div>
                                                </div>
                                        </div>
                                        <h6>Sales Representative</h6>
                                        <div class="b-personal__worker-name s-lineDownLeft">
                                                <h4 class="s-titleDet">Maria Susan</h4>
                                        </div>
                                        <p>Owner of AutoClub, started the company
                                                in 2005. A very hard working person who loves to drive the cars.</p>
                                </div>
                        </div></div><div class="owl-item" style="width: 285px;"><div>
                                <div class="b-personal__worker wow zoomInUp" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">
                                        <div class="b-personal__worker-img">
                                                <img src="<?= base_url() ?>assets/images/about/jason.jpg" class="img-responsive" alt="jason">
                                                <div class="b-personal__worker-img-social">
                                                        <div class="b-personal__worker-img-social-main">
                                                                <a href="#"><span class="fa fa-facebook-square"></span></a>
                                                                <a href="#"><span class="fa fa-twitter-square"></span></a>
                                                                <a href="#"><span class="fa fa-pinterest-square"></span></a>
                                                                <a href="#"><span class="fa fa-google-plus-square"></span></a>
                                                        </div>
                                                </div>
                                        </div>
                                        <h6>Owner / Co-Invester</h6>
                                        <div class="b-personal__worker-name s-lineDownLeft">
                                                <h4 class="s-titleDet">Jason Roy</h4>
                                        </div>
                                        <p>Owner of AutoClub, started the company
                                                in 2005. A very hard working person who loves to drive the cars.</p>
                                </div>
                        </div></div><div class="owl-item" style="width: 285px;"><div>
                                <div class="b-personal__worker wow zoomInUp" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">
                                        <div class="b-personal__worker-img">
                                                <img src="<?= base_url() ?>assets/images/about/paul.jpg" class="img-responsive" alt="paul">
                                                <div class="b-personal__worker-img-social">
                                                        <div class="b-personal__worker-img-social-main">
                                                                <a href="#"><span class="fa fa-facebook-square"></span></a>
                                                                <a href="#"><span class="fa fa-twitter-square"></span></a>
                                                                <a href="#"><span class="fa fa-pinterest-square"></span></a>
                                                                <a href="#"><span class="fa fa-google-plus-square"></span></a>
                                                        </div>
                                                </div>
                                        </div>
                                        <h6>Delivery Incharge</h6>
                                        <div class="b-personal__worker-name s-lineDownLeft">
                                                <h4 class="s-titleDet">Paul Richard</h4>
                                        </div>
                                        <p>Owner of AutoClub, started the company
                                                in 2005. A very hard working person who loves to drive the cars.</p>
                                </div>
                        </div></div><div class="owl-item" style="width: 285px;"><div>
                                <div class="b-personal__worker wow zoomInUp" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">
                                        <div class="b-personal__worker-img">
                                                <img src="<?= base_url() ?>assets/images/about/peter.jpg" class="img-responsive" alt="peter">
                                                <div class="b-personal__worker-img-social">
                                                        <div class="b-personal__worker-img-social-main">
                                                                <a href="#"><span class="fa fa-facebook-square"></span></a>
                                                                <a href="#"><span class="fa fa-twitter-square"></span></a>
                                                                <a href="#"><span class="fa fa-pinterest-square"></span></a>
                                                                <a href="#"><span class="fa fa-google-plus-square"></span></a>
                                                        </div>
                                                </div>
                                        </div>
                                        <h6>Auto Reviewer / Technican</h6>
                                        <div class="b-personal__worker-name s-lineDownLeft">
                                                <h4 class="s-titleDet">Peter Harold</h4>
                                        </div>
                                        <p>Owner of AutoClub, started the company
                                                in 2005. A very hard working person who loves to drive the cars.</p>
                                </div>
                        </div></div><div class="owl-item" style="width: 285px;"><div>
                                <div class="b-personal__worker wow zoomInUp" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">
                                        <div class="b-personal__worker-img">
                                                <img src="<?= base_url() ?>assets/images/about/maria.jpg" class="img-responsive" alt="maria">
                                                <div class="b-personal__worker-img-social">
                                                        <div class="b-personal__worker-img-social-main">
                                                                <a href="#"><span class="fa fa-facebook-square"></span></a>
                                                                <a href="#"><span class="fa fa-twitter-square"></span></a>
                                                                <a href="#"><span class="fa fa-pinterest-square"></span></a>
                                                                <a href="#"><span class="fa fa-google-plus-square"></span></a>
                                                        </div>
                                                </div>
                                        </div>
                                        <h6>Sales Representative</h6>
                                        <div class="b-personal__worker-name s-lineDownLeft">
                                                <h4 class="s-titleDet">Maria Susan</h4>
                                        </div>
                                        <p>Owner of AutoClub, started the company
                                                in 2005. A very hard working person who loves to drive the cars.</p>
                                </div>
                        </div></div></div></div>







                <div class="owl-controls clickable"><div class="owl-buttons"><div class="owl-prev"></div><div class="owl-next"></div></div></div></div>
        </div>
</section><!--b-personal-->

