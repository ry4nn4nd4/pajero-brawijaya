                        <div class="col-lg-9 col-md-8 col-sm-7 col-xs-6">
                                <div class="b-submit__main">
                                        <header class="s-headerSubmit s-lineDownLeft wow zoomInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">
                                                <h2 class="">Lengkapi informasi data pribadi Anda</h2>
                                        </header>
                                        <!--<p class="wow zoomInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">Curabitur libero. Donec facilisis velit eu est. Phasellus cons quat. Aenean vitae quam. Vivamus et nunc. Nunc consequsem velde metus imperdiet lacinia. Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>-->
                                        <form class="s-submit clearfix" action="<?= base_url() ?>account/register3" method="POST">
                                                <input type="hidden" name="car" value='<?= str_replace("'", "", $car)?>'>
                                                <div class="row">
                                                        <div class="col-md-6 col-xs-12">
<!--                                                                <div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">
                                                                        <input type="checkbox" name="check1" id="check1">
                                                                        <label class="s-submitCheckLabel" for="check1"><span class="fa fa-check"></span></label>
                                                                        <label class="s-submitCheck" for="check1">ABS</label>
                                                                </div>                               -->
                                                                <div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                                        <label>Nama<span>*</span></label>
                                                                        <input placeholder="Ketikkan nama Anda" type="text" name="name" value="<?=($mode == "test"?"Ryan Nanda":"")?>">
                                                                </div>
                                                                <div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                                        <label>Nama Panggilan<span>*</span></label>
                                                                        <input placeholder="Ketikkan nama panggilan Anda" type="text" name="call_name" value="<?=($mode == "test"?"Ryan":"")?>">
                                                                </div>
                                                                <div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                                        <label>Alamat<span>*</span></label>
                                                                        <input placeholder="Ketikkan alamat Anda" type="text" name="address" value="<?=($mode == "test"?"Alamat":"")?>">
                                                                </div>
                                                                <div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                                        <label>Nomor Telepon<span>*</span></label>
                                                                        <input placeholder="Ketikkan nomor telepon Anda" type="text" name="phone" value="<?=($mode == "test"?"081232646633":"")?>">
                                                                </div>
                                                                <div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                                        <label>Email<span>*</span></label>
                                                                        <input placeholder="Ketikkan email Anda" type="text" name="email" value="<?=($mode == "test"?"ry4nn4nd4@gmail.com":"")?>">
                                                                </div>
                                                                <div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                                        <label>Nomor Identitas KTP<span>*</span></label>
                                                                        <input placeholder="Ketikkan nomor identitas KTP Anda" type="text" name="id_number" value="<?=($mode == "test"?"3578091706890001":"")?>">
                                                                </div>
                                                        </div>
                                                        <div class="col-md-6 col-xs-12">
<!--                                                                <div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">
                                                                        <input type="checkbox" name="check13" id="check13">
                                                                        <label class="s-submitCheckLabel" for="check13"><span class="fa fa-check"></span></label>
                                                                        <label class="s-submitCheck" for="check13">Engine Immobiliser</label>
                                                                </div>-->
                                                                <div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                                        <label>Tempat Lahir<span>*</span></label>
                                                                        <input placeholder="Ketikkan tempat lahir Anda" type="text" name="place_of_birth" value="<?=($mode == "test"?"surabaya":"")?>">
                                                                </div>
                                                                <div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                                        <label>Tanggal Lahir<span>*</span></label>
                                                                        <input placeholder="Ketikkan tanggal lahir Anda" type="text" name="birthdate" value="<?=($mode == "test"?"1989-06-17":"")?>">
                                                                </div>
                                                                <div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                                        <label>Jenis Kelamin<span>*</span></label>
                                                                        <div class="s-relative">
                                                                                <select class="m-select" name="gender">
                                                                                        <option value="pria">Pria</option>
                                                                                        <option value="wanita">Wanita</option>
                                                                                </select>
                                                                                <span class="fa fa-caret-down"></span>
                                                                        </div>
                                                                        <!--<input placeholder="Ketikkan jenis_kelamin Anda" type="text" name="gender">-->
                                                                </div>
                                                                <div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                                        <label>Status Perkawinan<span>*</span></label>
                                                                        <div class="s-relative">
                                                                                <select class="m-select" name="marital_status">
                                                                                        <option value="menikah">Menikah</option>
                                                                                        <option value="belum menikah">Belum Menikah</option>
                                                                                </select>
                                                                                <span class="fa fa-caret-down"></span>
                                                                        </div>
                                                                        <!--<input placeholder="Ketikkan status perkawinan Anda" type="text" name="marital_status">-->
                                                                </div>
                                                                <div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                                        <label>Agama<span>*</span></label>
                                                                        <div class="s-relative">
                                                                                <select class="m-select" name="religion">
                                                                                        <option value="islam">Islam</option>
                                                                                        <option value="kristen">Kristen</option>
                                                                                        <option value="katolik">Katolik</option>
                                                                                        <option value="hindu">Hindu</option>
                                                                                        <option value="budha">Budha</option>
                                                                                </select>
                                                                                <span class="fa fa-caret-down"></span>
                                                                        </div>
                                                                        <!--<input placeholder="Ketikkan agama Anda" type="text" name="religion">-->
                                                                </div>
                                                                <div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                                        <label>Nomor Anggota<span>*</span></label>
                                                                        <input placeholder="Ketikkan nomor anggota Anda" type="text" name="id_card_number" value="<?=($mode == "test"?"a1b2c3d4e5f6":"")?>">
                                                                </div>
                                                        </div>
                                                </div>
                                                <button type="submit" class="btn m-btn pull-right wow zoomInUp" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">Lanjutkan Proses<span class="fa fa-angle-right"></span></button>
                                        </form>
                                </div>
                        </div>
                </div>
        </div>
</div><!--b-submit-->