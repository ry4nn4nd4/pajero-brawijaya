                        <div class="col-lg-9 col-md-8 col-sm-7 col-xs-6">
                                <div class="b-submit__main">
                                        <form action="<?= base_url() ?>account/join" method="post" class="s-submit clearfix">
                                                <div class="b-submit__main-contacts wow zoomInUp" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">
                                                        <header class="s-headerSubmit s-lineDownLeft">
                                                                <h2>Pengaturan nama akun & kata sandi</h2>
                                                        </header>
                                                        <p>Nama akun dan password dibawah ini akan Anda gunakan sebagai login ke Aplikasi</p>
<!--                                                        <div class="b-submit__main-element">
                                                                <label>Vehicle Current Address <span>*</span></label>
                                                                <input type="text" name="addr">
                                                        </div>
                                                        <div class="row">
                                                                <div class="col-md-6 col-xs-12">
                                                                        <div class="b-submit__main-element">
                                                                                <label>City / State  <span>*</span></label>
                                                                                <input type="text" name="city">
                                                                        </div>
                                                                </div>
                                                                <div class="col-md-6 col-xs-12">
                                                                        <div class="b-submit__main-element">
                                                                                <label>Zip Code / Country <span>*</span></label>
                                                                                <input type="text" name="code">
                                                                        </div>
                                                                </div>
                                                        </div>
                                                        <div class="b-submit__main-contacts-check m-check">
                                                                <input type="checkbox" name="check3" id="check3">
                                                                <label class="s-submitCheckLabel" for="check3"><span class="fa fa-check"></span></label>
                                                                <label class="s-submitCheck" for="check3">Show Location On Google Maps to Buyers</label>
                                                        </div>-->                                           <input type="hidden" name="member_id" value='<?= str_replace("'", "", $member_id)?>'>
                                                        <div class="row">
                                                                <div class="col-md-6 col-xs-12">
                                                                        <div class="b-submit__main-element">
                                                                                <label>Nama Akun<span>*</span></label>
                                                                                <input placeholder="Ketikkan nama akun Anda" type="text" name="username">
                                                                        </div>
                                                                </div>
                                                                <div class="col-md-6 col-xs-12">
                                                                        <div class="b-submit__main-element">
                                                                                <label>Kata Sandi<span>*</span></label>
                                                                                <input placeholder="Ketikkan kata sandi Anda" type="password" name="password">
                                                                        </div>
                                                                </div>
                                                        </div>
                                                </div>
                                                <button type="submit" class="btn m-btn pull-right wow zoomInUp" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">Simpan Data<span class="fa fa-angle-right"></span></button>
                                        </form>
                                </div>
                        </div>
                </div>
        </div>
</div><!--b-submit-->