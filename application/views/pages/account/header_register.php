<!--<section class="b-pageHeader">
        <div class="container">
                <h1 class="wow zoomInLeft" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInLeft;">Pendaftaran</h1>
                <div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInRight;">
                        <h3>Bergabunglah dengan kami</h3>
                </div>
        </div>
</section>b-pageHeader-->

<div class="b-breadCumbs s-shadow">
        <div class="container wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                <a href="<?= base_url() ?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span>
                <a href="<?= base_url() ?>account/register1" class="b-breadCumbs__page m-active">Daftar</a>
        </div>
</div><!--b-breadCumbs-->

<div class="b-infoBar">
        <div class="container">
                <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                                <div class="b-infoBar__progress wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                        <div class="b-infoBar__progress-line clearfix">
                                                <div class="b-infoBar__progress-line-step">
                                                        <div class="b-infoBar__progress-line-step-circle">
                                                                <div class="b-infoBar__progress-line-step-circle-inner m-active"></div>
                                                        </div>
                                                </div>
                                                <div class="b-infoBar__progress-line-step">
                                                        <div class="b-infoBar__progress-line-step-circle">
                                                                <div class="b-infoBar__progress-line-step-circle-inner"></div>
                                                        </div>
                                                </div>
                                                <div class="b-infoBar__progress-line-step">
                                                        <div class="b-infoBar__progress-line-step-circle">
                                                                <div class="b-infoBar__progress-line-step-circle-inner"></div>
                                                        </div>
                                                </div>
                                                <div class="b-infoBar__progress-line-step">
                                                        <div class="b-infoBar__progress-line-step-circle">
                                                                <div class="b-infoBar__progress-line-step-circle-inner"></div>
                                                        </div>
                                                        <div class="b-infoBar__progress-line-step-circle m-last">
                                                                <div class="b-infoBar__progress-line-step-circle-inner"></div>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</div><!--b-infoBar-->

<div class="b-submit">
        <div class="container">
                <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-5 col-xs-6">
                                <aside class="b-submit__aside">
                                        <div class="b-submit__aside-step m-active wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                <h3>Step 1</h3>
                                                <div class="b-submit__aside-step-inner m-active clearfix">
                                                        <div class="b-submit__aside-step-inner-icon">
                                                                <span class="fa fa-car"></span>
                                                        </div>
                                                        <div class="b-submit__aside-step-inner-info">
                                                                <h4>Data Mobil</h4>
                                                                <p>Lengkapi data mobil Anda</p>
                                                                <div class="b-submit__aside-step-inner-info-triangle"></div>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="b-submit__aside-step wow zoomInUp hidden-xs" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                <h3>Step 2</h3>
                                                <div class="b-submit__aside-step-inner clearfix">
                                                        <div class="b-submit__aside-step-inner-icon">
                                                                <span class="fa fa-user"></span>
                                                        </div>
                                                        <div class="b-submit__aside-step-inner-info">
                                                                <h4>Data Personal</h4>
                                                                <p>Lengkapi data diri Anda</p>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="b-submit__aside-step wow zoomInUp hidden-xs" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                <h3>Step 3</h3>
                                                <div class="b-submit__aside-step-inner clearfix">
                                                        <div class="b-submit__aside-step-inner-icon">
                                                                <span class="fa fa-list-ul"></span>
                                                        </div>
                                                        <div class="b-submit__aside-step-inner-info">
                                                                <h4>Data Pekerjaan</h4>
                                                                <p>Lengkapi data pekerjaan Anda</p>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="b-submit__aside-step wow zoomInUp hidden-xs" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                <h3>Step 4</h3>
                                                <div class="b-submit__aside-step-inner clearfix">
                                                        <div class="b-submit__aside-step-inner-icon">
                                                                <span class="fa fa-photo"></span>
                                                        </div>
                                                        <div class="b-submit__aside-step-inner-info">
                                                                <h4>Photos &amp; videos</h4>
                                                                <p>Lengkapi data photo &amp; video</p>
                                                        </div>
                                                </div>
                                        </div>
                                        
                                        <div class="b-submit__aside-step wow zoomInUp hidden-xs" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                <h3>Step 5</h3>
                                                <div class="b-submit__aside-step-inner clearfix">
                                                        <div class="b-submit__aside-step-inner-icon">
                                                                <span class="fa fa-globe"></span>
                                                        </div>
                                                        <div class="b-submit__aside-step-inner-info">
                                                                <h4>VERIFIKASI DATA</h4>
                                                                <p>Proses verifikasi data Anda </p>
                                                        </div>
                                                </div>
                                        </div>
                                </aside>
                        </div>