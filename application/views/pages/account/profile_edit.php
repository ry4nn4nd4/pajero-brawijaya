<link href="<?= base_url() ?>assets/css/account-profile.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/font-awesome-5.1.min.css" rel="stylesheet">

    <div class="rela-block container-section">
        <div class="rela-block profile-card">
            <div class="profile-pic" style="background: url('<?=$imagePath.$member_photo?>') center no-repeat;" id="profile_pic"></div>
            <form class="s-submit clearfix" action="<?= base_url() ?>account/profile_edit/<?=$userId?>" method="POST">
                <div class="rela-block profile-name-container-center">
                    <div class="b-submit__main-element wow zoomInUp">
                            <label>Nama Panggilan <span>*</span></label>
                            <input placeholder="Nama Panggilan Anda" type="text" name="member_call_name" value="<?=$call_name?>">
                    </div>
                    <div class="b-submit__main-element wow zoomInUp">
                            <label>Yang Anda Pikirkan <span>*</span></label>
                            <input placeholder="Apa yg sedang Anda Pikirkan?" type="text" name="member_activity_status" value="<?=$activity_status?>">
                    </div>
                </div>
                <div class="rela-block profile-name-container">
                    <div class="section-title-right" style="margin-top: 0px;">
                        <span class="section-subtitle">Data Pribadi</span>
                    </div>

                        <input type="hidden" name="member_id" value='<?= str_replace("'", "", $userId)?>'>
                        <div class="b-submit__main-element wow zoomInUp">
                                <label>Nama <span>*</span></label>
                                <input placeholder="Ketikkan Nama Anda" type="text" name="member_name" value="<?=$member_name?>">
                        </div>
                        <div class="b-submit__main-element wow zoomInUp">
                                <label>No Handphone <span>*</span></label>
                                <input placeholder="Ketikkan No Handphone Anda" type="text" name="member_phone" value="<?=$phone?>">
                        </div>
                        <div class="b-submit__main-element wow zoomInUp">
                                <label>Email <span>*</span></label>
                                <input placeholder="Ketikkan Email Anda" type="text" name="member_email" value="<?=$email?>">
                        </div>
                        <div class="b-submit__main-element wow zoomInUp">
                                <label>Alamat <span>*</span></label>
                                <input placeholder="Ketikkan Alamat Anda" type="text" name="member_address" value="<?=$member_address?>">
                        </div>
                        <div class="b-submit__main-element wow zoomInUp">
                                <label>Id Card <span>*</span></label>
                                <input placeholder="Ketikkan Id Card Anda" type="text" name="member_id_card_number" value="<?=$id_card_number?>">
                        </div>

                        <div class="section-title-right">
                            <span class="section-subtitle">Data Pekerjaan</span>
                        </div>
                        <div class="b-submit__main-element wow zoomInUp">
                                <label>Nama Pekerjaan <span>*</span></label>
                                <input placeholder="Ketikkan Nama Pekerjaan Anda" type="text" name="company_work" value="<?=$work?>">
                        </div>
                        <div class="b-submit__main-element wow zoomInUp">
                                <label>Tipe Pekerjaan <span>*</span></label>
                                <input placeholder="Ketikkan Tipe Pekerjaan Anda" type="text" name="company_type" value="<?=$type?>">
                        </div>
                        <div class="b-submit__main-element wow zoomInUp">
                                <label>Nama Perusahaan <span>*</span></label>
                                <input placeholder="Ketikkan Nama Perusahaan Anda" type="text" name="company_name" value="<?=$company_name?>">
                        </div>
                        <div class="b-submit__main-element wow zoomInUp">
                                <label>Alamat Perusahaan <span>*</span></label>
                                <input placeholder="Ketikkan Alamat Perusahaan Anda" type="text" name="company_address" value="<?=$company_address?>">
                        </div>

                        <div class="section-title-right">
                            <span class="section-subtitle">Data Mobil</span>
                        </div>
                        <div class="b-submit__main-element wow zoomInUp" style="text-indent: 0px !important;visibility: visible;animation-name: zoomInUp;">
                                <label>Tipe <span>*</span></label>
                                <div class="s-relative">
                                        <select class="m-select" name="car_type">
                                                <option <?=($car_type=="DAKAR 4x2"?"selected":"")?> >DAKAR 4x2</option>
                                                <option <?=($car_type=="DAKAR 4x4"?"selected":"")?> >DAKAR 4x4</option>
                                                <option <?=($car_type=="EXCEED 4x2"?"selected":"")?> >EXCEED 4x2</option>
                                                <option <?=($car_type=="GLS 4x2"?"selected":"")?> >GLS 4x2</option>
                                                <option <?=($car_type=="GLX 4x4"?"selected":"")?> >GLX 4x4</option>
                                        </select>
                                        <span class="fa fa-caret-down"></span>
                                </div>
                        </div>
                        <div class="b-submit__main-element wow zoomInUp">
                                <label>Tahun <span>*</span></label>
                                <input placeholder="Ketikkan Tahun Mobil Anda" type="text" name="car_year" value="<?=$year?>">
                        </div>
                        <div class="b-submit__main-element wow zoomInUp">
                                <label>Warna <span>*</span></label>
                                <input placeholder="Ketikkan Warna Mobil Anda" type="text" name="car_color" value="<?=$color?>">
                        </div>
                        <div class="b-submit__main-element wow zoomInUp">
                                <label>No Polisi <span>*</span></label>
                                <input placeholder="Ketikkan No Polisi Mobil Anda" type="text" name="car_police_number" value="<?=$police_number?>">
                        </div>
                        <div class="b-submit__main-element wow zoomInUp">
                                <label>No Mesin <span>*</span></label>
                                <input placeholder="Ketikkan No Mesin Mobil Anda" type="text" name="car_machine_number" value="<?=$machine_number?>">
                        </div>
                        <div class="b-submit__main-element wow zoomInUp">
                                <label>No Rangka <span>*</span></label>
                                <input placeholder="Ketikkan No Rangka Mobil Anda" type="text" name="car_body_number" value="<?=$body_number?>">
                        </div>
                        <button type="submit" class="btn m-btn pull-right wow zoomInUp" data-wow-delay="0.3s" style="margin-top: 0px;margin-bottom: 25px;">Perbarui Data<span class="fa fa-sm fa-heart"></span></button>
                </div>
            </form>
            <div class="rela-block profile-card-stats">
                <div class="floated profile-stat works" id="num_works">28<br></div>
                <div class="floated profile-stat followers" id="num_followers">112<br></div>
                <div class="floated profile-stat following" id="num_following">245<br></div>
            </div>
        </div>            
    </div>
