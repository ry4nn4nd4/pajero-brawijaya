<div class="b-submit">
        <div class="container">
                <div class="row">
                        <div class="col-lg-9 col-md-8 col-sm-7 col-xs-6">
                                <div class="b-submit__main">
                                        <form action="<?= base_url() ?>account/profile_pin/<?=$userId?>" method="post" class="s-submit clearfix">
                                                <div class="b-submit__main-contacts wow zoomInUp" data-wow-delay="0.3s" style="animation-delay: 0.3s; animation-name: none;">
                                                        <header class="s-headerSubmit s-lineDownLeft">
                                                                <h2>Pengaturan nama akun & kata sandi</h2>
                                                        </header>
                                                        <?php
                                                            if(!isset($response) || $response == ""){
                                                        ?>
                                                            <p>Masukkan data yg dibuthukan dibawah ini</p>
                                                        <?php
                                                            }else{
                                                        ?>
                                                            <span class="text-danger fa fa-close" style="margin-bottom: 35px;"> <?=$response?></span>

                                                        <?php
                                                            }
                                                        ?>
                                                        
                                                        <input type="hidden" name="member_id" value='<?=$userId?>'>
                                                        <div class="row">
                                                                <div class="col-md-6 col-xs-12">
                                                                        <div class="b-submit__main-element">
                                                                                <label>Nama Akun <span>*</span></label>
                                                                                <input placeholder="Ketikkan nama akun Anda" type="text" name="username" value="<?=$username?>">
                                                                        </div>
                                                                </div>
                                                                <div class="col-md-6 col-xs-12">
                                                                        <div class="b-submit__main-element">
                                                                                <label>Kata Sandi Lama <span>*</span></label>
                                                                                <input placeholder="Ketikkan kata sandi lama Anda" type="password" name="old_password" value="<?=$old_password?>">
                                                                        </div>
                                                                </div>
                                                        </div>
                                                        <div class="row">
                                                                <div class="col-md-6 col-xs-12">
                                                                        <div class="b-submit__main-element">
                                                                                <label>Kata Sandi Baru <span>*</span></label>
                                                                                <input placeholder="Ketikkan kata sandi baru Anda" type="password" name="new_password" value="<?=$new_password?>">
                                                                        </div>
                                                                </div>
                                                                <div class="col-md-6 col-xs-12">
                                                                        <div class="b-submit__main-element">
                                                                                <label>Konfirmasi Kata Sandi Baru <span>*</span></label>
                                                                                <input placeholder="Ketikkan kembali kata sandi baru Anda" type="password" name="confirm_password" value="<?=$confirm_password?>">
                                                                        </div>
                                                                </div>
                                                        </div>
                                                </div>
                                                <button type="submit" class="btn m-btn pull-right wow zoomInUp" data-wow-delay="0.3s" style="animation-delay: 0.3s; animation-name: none;">Perbarui Pin<span class="fa fa-angle-right"></span></button>
                                        </form>
                                </div>
                        </div>
                </div>
        </div>
</div><!--b-submit-->