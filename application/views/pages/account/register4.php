<style type="text/css">
    .img-preview{
        margin-bottom: 20px;
        width: 100%;
    }
</style>
                            <div class="col-lg-9 col-md-8 col-sm-7 col-xs-6">
                                <div class="b-submit__main">
                                        <form action="<?= base_url() ?>account/register5" method="post" class="s-submit" enctype="multipart/form-data">
                                                <input type="hidden" name="member_id" value='<?= str_replace("'", "", $member_id)?>'>
                                                <div class="s-form">
                                                        <div class="b-submit__main-file">
                                                                <header class="s-headerSubmit s-lineDownLeft wow zoomInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">
                                                                        <h2>Upload foto Anda</h2>
                                                                </header>
                                                                <p class="wow zoomInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">Pilih foto terbaik Anda.</p>
                                                                <label class="b-submit__main-file-label btn m-btn wow zoomInUp" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">
                                                                        <input type="file" class="" id="member-file" name="member_photo[]">
                                                                        <span>Pilih Photo Anda</span>
                                                                        <span class="fa fa-angle-right"></span>
                                                                </label>
                                                                <div id="member-preview"></div>
                                                                <label>Max. ukuran file : 1 MB. Format photo: jpg, gif, png.</label>
                                                        </div>
                                                        <div class="b-submit__main-file">
                                                                <header class="s-headerSubmit s-lineDownLeft wow zoomInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">
                                                                        <h2>Upload foto mobil Anda</h2>
                                                                </header>
                                                                <p class="wow zoomInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">Pilih foto terbaik mobil Anda.</p>
                                                                <label class="b-submit__main-file-label btn m-btn wow zoomInUp" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">
                                                                        <input type="file" class="" id="car-file" name="car_photo[]">
                                                                        <span>Pilih Photo Mobil Anda</span>
                                                                        <span class="fa fa-angle-right"></span>
                                                                </label>
                                                                <div id="car-preview"></div>
                                                                <label>Max. ukuran file : 1 MB. Format photo: jpg, gif, png.</label>
                                                        </div>
<!--                                                        <div class="b-submit__main-file wow zoomInUp" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">
                                                                <header class="s-headerSubmit s-lineDownLeft">
                                                                        <h2>Please Enter A Hosted Video URL Of Your Vehicle</h2>
                                                                </header>
                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                                                <input type="text" placeholder="like Youtube or Vimeo URL" name="url">
                                                        </div>
                                                        <div class="b-submit__main-file wow zoomInUp" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">
                                                                <header class="s-headerSubmit s-lineDownLeft">
                                                                        <h2>Tuliskan </h2>
                                                                </header>
                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                                                <textarea name="text" placeholder="write additional comments"></textarea>
                                                        </div>-->
                                                </div>
                                                
                                                <button type="submit" class="btn m-btn pull-right wow zoomInUp" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">Lanjutkan Proses<span class="fa fa-angle-right"></span></button>
                                        </form>
                                </div>
                        </div>
                </div>
        </div>
</div><!--b-submit-->
<script type="text/javascript">
    
    $(document).ready(function(){
        $('#member-file').on('change', function(){ //on file input change
            if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
            {
                $('#car-preview').html(''); //clear html of output element
                var data = $(this)[0].files; //this file data

                $.each(data, function(index, file){ //loop though each file
                    if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){ //check supported file type
                        var fRead = new FileReader(); //new filereader
                        fRead.onload = (function(file){ //trigger function on successful read
                        return function(e) {
                            var img = $('<img/>').addClass('img-preview').attr('src', e.target.result); //create image element 
                            $('#member-preview').append(img); //append image to output element
                        };
                        })(file);
                        fRead.readAsDataURL(file); //URL representing the file's data.
                    }
                });
            }else{
                alert("Silahkan pilih photo Anda yg lain");
            }
        });
        
        $('#car-file').on('change', function(){ //on file input change
            if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
            {
                $('#car-preview').html(''); //clear html of output element
                var data = $(this)[0].files; //this file data

                $.each(data, function(index, file){ //loop though each file
                    if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){ //check supported file type
                        var fRead = new FileReader(); //new filereader
                        fRead.onload = (function(file){ //trigger function on successful read
                        return function(e) {
                            var img = $('<img/>').addClass('img-preview').attr('src', e.target.result); //create image element 
                            $('#car-preview').append(img); //append image to output element
                        };
                        })(file);
                        fRead.readAsDataURL(file); //URL representing the file's data.
                    }
                });
            }else{
                alert("Silahkan pilih photo mobil Anda yg lain"); //if File API is absent
            }
        });
    });

</script>