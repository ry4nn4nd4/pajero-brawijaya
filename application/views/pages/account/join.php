<div class="b-breadCumbs s-shadow">
        <div class="container wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                <a href="<?= base_url() ?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span>
                <a href="<?= base_url() ?>account/register1" class="b-breadCumbs__page m-active">Daftar</a>
        </div>
</div><!--b-breadCumbs-->
<div class="row">
    <div class="col-xs-12">
        <div class="" style="margin:35px 25px 75px 25px;">
            <center><span class="fa fa-lg fa-check-circle" style="font-size: 10em; color:lightgreen;margin:10px;"></span></center>
            <center><span class="" style="font-family: Roboto; font-size: 20px; font-weight: bold;">Terimakasih !</span></center>
            <center><span class="">Akun Anda akan segera aktif setelah proses verifikasi oleh tim kami.</span></center>
        </div>
    </div>
</div>