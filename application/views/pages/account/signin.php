<section class="b-pageHeader">
        <div class="container">
                <h1 class="wow zoomInLeft" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInLeft;">Sign In</h1>
                <div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInRight;">
                        <h3>Sign in with your account </h3>
                </div>
        </div>
</section><!--b-pageHeader-->

<div class="b-breadCumbs s-shadow">
        <div class="container wow zoomInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">
                <a href="<?= base_url() ?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span>
                <a href="<?= base_url() ?>account/signin" class="b-breadCumbs__page m-active">Sign In</a>
        </div>
</div><!--b-breadCumbs-->

<div class="b-submit">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-3 col-xs-3 hidden-xs"></div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                <div class="b-submit__main">
                    <form action="<?= base_url() ?>account/signin" method="post">
                        <div class="s-form">
                            <div class="b-submit__main-file wow zoomInUp" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">
                                <header class="s-headerSubmit s-lineDownLeft">
                                        <h2>Member Area</h2>
                                </header>
                                <?php
                                    if(!isset($response) || $response == ""){
                                ?>
                                    <p>Masukkan nama akun dan kata sandi Anda</p>
                                <?php
                                    }else{
                                ?>
                                    <span class="text-danger fa fa-close" style="margin-bottom: 35px;"> <?=$response?></span>
                                    
                                <?php
                                    }
                                ?>
                                
                                <input type="text" placeholder="Nama Akun" name="username" value="<?=$username?>">
                                <input type="password" placeholder="Kata Sandi" name="password" value="<?=$password?>">
                            </div>
                        </div>
                        <div class="s-submit">
                            <button type="submit" class="btn m-btn pull-right wow zoomInUp" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">Sign In Now <span class="fa fa-angle-right"></span></button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-84col-md-4 col-sm-3 col-xs-3 hidden-xs"></div>
        </div>
    </div>
</div><!--b-submit-->