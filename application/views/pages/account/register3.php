                        <div class="col-lg-9 col-md-8 col-sm-7 col-xs-6">
                                <div class="b-submit__main">
                                        <header class="s-headerSubmit s-lineDownLeft wow zoomInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">
                                                <h2 class="">Lengkapi informasi data pekerjaan Anda</h2>
                                        </header>
                                        <form class="s-submit clearfix" action="<?= base_url() ?>account/register4" method="post">
                                                <input type="hidden" name="car" value='<?= str_replace("'", "", $car)?>'>
                                                <input type="hidden" name="member" value='<?= str_replace("'", "", $member)?>'>
                                                <div class="row">
                                                        <div class="col-md-6 col-xs-12">
<!--                                                                <div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">
                                                                        <input type="checkbox" name="check1" id="check1">
                                                                        <label class="s-submitCheckLabel" for="check1"><span class="fa fa-check"></span></label>
                                                                        <label class="s-submitCheck" for="check1">ABS</label>
                                                                </div>                               -->
                                                                <div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                                        <label>Pekerjaan<span>*</span></label>
                                                                        <input placeholder="Ketikkan pekerjaan Anda" type="text" name="work" value="<?=($mode == "test"?"Karyawan":"")?>">
                                                                </div>
                                                                <div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                                        <label>Bidang Usaha<span>*</span></label>
                                                                        <input placeholder="Ketikkan bidang usaha pekerjaan Anda" type="text" name="type" value="<?=($mode == "test"?"IT":"")?>">
                                                                </div>
                                                        </div>
                                                        <div class="col-md-6 col-xs-12">
<!--                                                                <div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">
                                                                        <input type="checkbox" name="check13" id="check13">
                                                                        <label class="s-submitCheckLabel" for="check13"><span class="fa fa-check"></span></label>
                                                                        <label class="s-submitCheck" for="check13">Engine Immobiliser</label>
                                                                </div>-->
                                                                <div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                                        <label>Nama Kantor<span>*</span></label>
                                                                        <input placeholder="Ketikkan nama kantor Anda" type="text" name="name" value="<?=($mode == "test"?"PT. ABC":"")?>">
                                                                </div>
                                                                <div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                                        <label>Alamat Kantor<span>*</span></label>
                                                                        <input placeholder="Ketikkan alamat kantor Anda" type="text" name="address" value="<?=($mode == "test"?"Semampir Selatan":"")?>">
                                                                </div>
                                                        </div>
                                                </div>
                                                <button type="submit" class="btn m-btn pull-right wow zoomInUp" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">Lanjutkan Proses<span class="fa fa-angle-right"></span></button>
                                        </form>
                                </div>
                        </div>
                </div>
        </div>
</div><!--b-submit-->