
                        <div class="col-lg-9 col-md-8 col-sm-7 col-xs-6">
                                <div class="b-submit__main">
                                        <header class="s-headerSubmit s-lineDownLeft wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                <h2 class="">Lengkapi informasi data mobil Anda</h2>
                                        </header>
                                        <form class="s-submit clearfix" action="<?= base_url() ?>account/register2" method="POST">
                                                <div class="row">
                                                        <div class="col-md-6 col-xs-12">
                                                                <div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                                        <label>Tipe<span>*</span></label>
                                                                        <div class="s-relative">
                                                                                <select class="m-select" name="type">
                                                                                        <option>DAKAR 4x2</option>
                                                                                        <option>DAKAR 4x4</option>
                                                                                        <option>EXCEED 4x2</option>
                                                                                        <option>GLS 4x2</option>
                                                                                        <option>GLX 4x4</option>
                                                                                </select>
                                                                                <span class="fa fa-caret-down"></span>
                                                                        </div>
                                                                </div>
                                                                <div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                                        <label>Warna<span>*</span></label>
                                                                        <input placeholder="Ketikkan warna mobil Anda" type="text" name="color" value="<?=($mode == "test"?"putih":"")?>">
                                                                </div>
                                                                <div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                                        <label>Nomor Rangka<span>*</span></label>
                                                                        <input placeholder="Ketikkan nomor rangka mobil Anda" type="text" name="body_number" value="<?=($mode == "test"?"123456":"")?>">
                                                                </div>
                                                        </div>
                                                        <div class="col-md-6 col-xs-12">
                                                                <div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                                        <label>Tahun Pembuatan<span>*</span></label>
                                                                        <input placeholder="Ketikkan tahun pembuatan mobil Anda" type="text" name="year" value="<?=($mode == "test"?"2010":"")?>">
                                                                </div>
                                                                <div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                                        <label>Nomor Polisi<span>*</span></label>
                                                                        <input placeholder="Ketikkan nomor polisi mobil Anda" type="text" name="police_number" value="<?=($mode == "test"?"L 1771 D":"")?>">
                                                                </div>
                                                                <div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                                        <label>Nomor Mesin<span>*</span></label>
                                                                        <input placeholder="Ketikkan nomor mesin mobil Anda" type="text" name="machine_number" value="<?=($mode == "test"?"abcdef":"")?>">
                                                                </div>
                                                        </div>
                                                </div>
                                                <button type="submit" class="btn m-btn pull-right wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">Lanjutkan Proses<span class="fa fa-angle-right"></span></button>
                                        </form>
                                </div>
                        </div>
                </div>
        </div>
</div><!--b-submit-->