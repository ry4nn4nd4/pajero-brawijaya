<link href="<?= base_url() ?>assets/css/account-profile.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/font-awesome-5.1.min.css" rel="stylesheet">

<style>
    /*flyout*/
    #profile, #photo, #pin{
        position: absolute;
        z-index: -1;
        width: 100%;        
        color: white;
        border-radius: 50px;
        border-color: #46b8da;
        transition: 0.5s;
        visibility: hidden;
        overflow: hidden;
        box-shadow: 2px 2px silver;
    }
    #profile.expanded{
        background-color : #0074D9;
        position:absolute;
        z-index:-1;
        top:-180px;
        width:55px;
        height:55px;
        visibility:visible;
        text-align: center;
        padding-top: 16px;
        animation-name: zoomInUp;
    }
    #photo.expanded{
        background-color : #F012BE;
        position:absolute;
        z-index:-1;
        top:-120px;
        width:55px;
        height:55px;
        visibility:visible;
        text-align: center;
        padding-top: 17px;
        animation-name: zoomInUp;
    }
    #pin.expanded{
        background-color : #B10DC9;
        position:absolute;
        z-index:-1;
        top:-60px;
        width:55px;
        height:55px;
        visibility:visible;
        text-align: center;
        padding-top: 16px;
        animation-name: zoomInUp;
    }
</style>

    <div class="rela-block container-section">
        <div class="rela-block profile-card">
            <div class="profile-pic" style="background: url('<?=$imagePath.$member_photo?>') center no-repeat;" id="profile_pic"></div>
            <div class="rela-block profile-name-container-center">
                <div class="rela-block user-name" id="user_name"><?=$call_name?></div>
                <div class="rela-block user-desc" id="user_description"><?=$activity_status?></div>
            </div>
            <div class="rela-block profile-name-container">
                <div class="section-title-right" style="margin-top: 20px;">
                    <span class="section-subtitle">Data Pribadi</span>
                </div>
                
                <span class="user-desc"><i class="fa fa-user pull-left pad-right-15"></i><?=($member_name == "") ? $defaultNull : $member_name;?></span><hr class="line">
                <span class="user-desc"><i class="fa fa-phone pull-left pad-right-15"></i><?=($phone == "") ? $defaultNull : $phone;?></span><hr class="line">
                <span class="user-desc"><i class="fa fa-envelope pull-left pad-right-15"></i><?=($email == "") ? $defaultNull : $email;?></span><hr class="line">
                <span class="user-desc"><i class="fa fa-location-arrow pull-left pad-right-15"></i><?=($member_address == "") ? $defaultNull : $member_address;?></span><hr class="line">
                <span class="user-desc"><i class="fa fa-id-badge pull-left pad-right-15"></i>&nbsp;<?=($id_card_number == "") ? $defaultNull : $id_card_number;?></span>
                
                <div class="section-title-right">
                    <span class="section-subtitle">Data Pekerjaan</span>
                </div>
                <span class="user-desc"><i class="fa fa-briefcase pull-left pad-right-15"></i><?=($work == "") ? $defaultNull : $work;?></span><hr class="line">
                <span class="user-desc"><i class="fa fa-book pull-left pad-right-15"></i><?=($type == "") ? $defaultNull : $type;?></span><hr class="line">
                <span class="user-desc"><i class="fa fa-building pull-left pad-right-15"></i><?=($company_name == "") ? $defaultNull : $company_name;?></span><hr class="line">
                <span class="user-desc"><i class="fa fa-location-arrow pull-left pad-right-15"></i><?=($company_address == "") ? $defaultNull : $company_address;?></span>
                
                <div class="section-title-right">
                    <span class="section-subtitle">Data Mobil</span>
                </div>
                <span class="user-desc"><i class="fa fa-car pull-left pad-right-15"></i><?=($model == "") ? "-" : $model . " - " . $car_type;?></span><hr class="line">
                <span class="user-desc"><i class="fa fa-calendar pull-left pad-right-15"></i><?=($year == "") ? $defaultNull : $year;?></span><hr class="line">
                <span class="user-desc"><i class="fa fa-paint-brush pull-left pad-right-15"></i><?=($color == "") ? $defaultNull : $color;?></span><hr class="line">
                <span class="user-desc"><i class="fa fa-id-card pull-left pad-right-15"></i><?=($police_number == "") ? $defaultNull : $police_number;?></span><hr class="line">
                <span class="user-desc"><i class="fa fa-id-card pull-left pad-right-15"></i><?=($machine_number == "") ? $defaultNull : $machine_number;?></span><hr class="line">
                <span class="user-desc"><i class="fa fa-id-card pull-left pad-right-15"></i><?=($body_number == "") ? $defaultNull : $body_number;?></span>
            
            </div>
            <div class="rela-block profile-card-stats">
                <div class="floated profile-stat works" id="num_works">28<br></div>
                <div class="floated profile-stat followers" id="num_followers">112<br></div>
                <div class="floated profile-stat following" id="num_following">245<br></div>
            </div>
        </div>
        <div id="editbutton">
            <div id="profile" onclick="window.location.href='<?= base_url() ?>account/profile_edit/<?=$userId?>'">
                <span class="fa fa-user"></span>
            </div>
            <div id="photo" onclick="window.location.href='<?= base_url() ?>account/profile_photo/<?=$userId?>'">
                <span class="fa fa-camera"></span>
            </div>
            <div id="pin" onclick="window.location.href='<?= base_url() ?>account/profile_pin/<?=$userId?>'">
                <span class="fa fa-lock"></span>
            </div>
            <div id="edit" class="edit-profile">
                <span class="fa fa-pencil-square-o"></span>
            </div>
        </div>
<!--        <div class="rela-block content">
            <div class="col-sm-3 col-xs-12">
                <div class="rela-inline image"></div>
            </div>
            <div class="col-sm-3 col-xs-12">
                <div class="rela-inline image"></div>
            </div>
            <div class="col-sm-3 col-xs-12">
                <div class="rela-inline image"></div>
            </div>
            <div class="col-sm-3 col-xs-12">
                <div class="rela-inline image"></div>
            </div>
        </div>-->
            
    </div>
<script>
    var edit = document.getElementById("edit");
    var profile = document.getElementById("profile");
    var photo = document.getElementById("photo");
    edit.addEventListener('click',expand);
    function collapse(e){
        profile.className='';
        photo.className='';
        pin.className='';
        edit.removeEventListener('click',collapse);
        edit.addEventListener('click',expand);
    }
    function expand(e){
        profile.className='expanded';
        photo.className='expanded';
        pin.className='expanded';
        edit.removeEventListener('click',expand);
        edit.addEventListener('click',collapse);
    }
</script>
