<section class="b-pageHeader">
        <div class="container">
                <h1 class="wow zoomInLeft" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInLeft;">Detail Kegiatan</h1>
        </div>
</section><!--b-pageHeader-->

<div class="b-breadCumbs s-shadow wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
        <div class="container">
                <a href="<?= base_url() ?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span>
                <a href="<?= base_url()."event/".$eventType ?>" class="b-breadCumbs__page m-active">Kegiatan</a><span class="fa fa-angle-right"></span>
                <a href="#" class="b-breadCumbs__page">Detail Kegiatan <?=$eventName?></a>
        </div>
</div><!--b-breadCumbs-->

<!--b-infoBar
<div class="b-infoBar">
        <div class="container">
                <div class="row wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                        <div class="col-xs-3">
                                <div class="b-infoBar__premium">Premium Listing</div>
                        </div>
                        <div class="col-xs-9">
                                <div class="b-infoBar__btns">
                                        <a href="http://templines.rocks/html/sokolcov/auto-club/detail.html#" class="btn m-btn m-infoBtn">SHARE THIS VEHICLE<span class="fa fa-angle-right"></span></a>
                                        <a href="http://templines.rocks/html/sokolcov/auto-club/detail.html#" class="btn m-btn m-infoBtn">ADD TO FAVOURITES<span class="fa fa-angle-right"></span></a>
                                        <a href="http://templines.rocks/html/sokolcov/auto-club/detail.html#" class="btn m-btn m-infoBtn">PRINT THIS PAGE<span class="fa fa-angle-right"></span></a>
                                        <a href="http://templines.rocks/html/sokolcov/auto-club/detail.html#" class="btn m-btn m-infoBtn">DOWNLOAD MANUAL<span class="fa fa-angle-right"></span></a>
                                </div>
                        </div>
                </div>
        </div>
</div>
b-infoBar-->

		<section class="b-detail s-shadow">
			<div class="container">
				<header class="b-detail__head s-lineDownLeft wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
					<div class="row">
						<div class="col-sm-9 col-xs-12">
							<div class="b-detail__head-title">
								<h1><?= $eventName?></h1>
								<h3><?= $eventTitle?></h3>
							</div>
						</div>
						<div class="col-sm-3 col-xs-12">
<!--							<div class="b-detail__head-price">
								<div class="b-detail__head-price-num"><?= masking_date($eventDate)?></div>
                                                                <p><?= $eventParticipant?> Anggota</p>
							</div>-->
<!--                                                        <div class="row" style="padding-top: 10px;">
                                                                <div class="col-xs-6">
                                                                    <h4 class="b-detail__main-aside-desc-title"><span>Kegiatan</span></h4>
                                                                </div>
                                                                <div class="col-xs-6">
                                                                        <p class="b-detail__main-aside-desc-value"><?= masking_date($eventDate)?></p>
                                                                </div>
                                                        </div>
                                                        <div class="row">
                                                                <div class="col-xs-6">
                                                                    <h4 class="b-detail__main-aside-desc-title"><span>Peserta</span></h4>
                                                                </div>
                                                                <div class="col-xs-6">
                                                                        <p class="b-detail__main-aside-desc-value"><?= $eventParticipant?> Anggota</p>
                                                                </div>
                                                        </div>-->
						</div>
					</div>
				</header>
				<div class="b-detail__main">
					<div class="row">
						<div class="col-md-8 col-xs-12">
							<div class="b-detail__main-info">
								<div class="b-detail__main-info-images wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
									<div class="row m-smallPadding">
										<div class="col-md-10 col-xs-12">
											<div class="bx-wrapper" style="max-width: 100%;">
                                                                                            <div class="bx-viewport" style="width: 100%; overflow: hidden; position: relative; height: auto;">
                                                                                                <ul class="b-detail__main-info-images-big bxslider enable-bx-slider" data-pager-custom="#bx-pager" data-mode="horizontal" data-pager-slide="true" data-mode-pager="vertical" data-pager-qty="5" style="width: 1015%; position: relative; transition-duration: 0s; transform: translate3d(-613px, 0px, 0px);">
                                                                                                    <li class="s-relative-bx bx-clone">                                        
                                                                                                            <a data-toggle="modal" data-target="#myModal" href="http://templines.rocks/html/sokolcov/auto-club/detail.html#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>VIDEO</a>
                                                                                                            <img class="img-responsive center-block" src="<?= base_url() ?>assets/images/detail/big3.jpg" alt="nissan">
                                                                                                    </li>
                                                                                                    <li class="s-relative-bx">                                        
                                                                                                            <a data-toggle="modal" data-target="#myModal" href="http://templines.rocks/html/sokolcov/auto-club/detail.html#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>VIDEO</a>
                                                                                                            <img class="img-responsive center-block" src="<?= base_url() ?>assets/images/detail/big1.jpg" alt="nissan">
                                                                                                    </li>
                                                                                                    <li class="s-relative-bx">                                        
                                                                                                            <a data-toggle="modal" data-target="#myModal" href="http://templines.rocks/html/sokolcov/auto-club/detail.html#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>VIDEO</a>
                                                                                                            <img class="img-responsive center-block" src="<?= base_url() ?>assets/images/detail/big2.jpg" alt="nissan">
                                                                                                    </li>
                                                                                                    <li class="s-relative-bx">                                        
                                                                                                            <a data-toggle="modal" data-target="#myModal" href="http://templines.rocks/html/sokolcov/auto-club/detail.html#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>VIDEO</a>
                                                                                                            <img class="img-responsive center-block" src="<?= base_url() ?>assets/images/detail/big3.jpg" alt="nissan">
                                                                                                    </li>
                                                                                                    <li class="s-relative-bx">                                        
                                                                                                            <a data-toggle="modal" data-target="#myModal" href="http://templines.rocks/html/sokolcov/auto-club/detail.html#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>VIDEO</a>
                                                                                                            <img class="img-responsive center-block" src="<?= base_url() ?>assets/images/detail/big4.jpg" alt="nissan">
                                                                                                    </li>
                                                                                                    <li class="s-relative-bx">                                        
                                                                                                            <a data-toggle="modal" data-target="#myModal" href="http://templines.rocks/html/sokolcov/auto-club/detail.html#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>VIDEO</a>
                                                                                                            <img class="img-responsive center-block" src="<?= base_url() ?>assets/images/detail/big5.jpg" alt="nissan">
                                                                                                    </li>
                                                                                                    <li class="s-relative-bx">                                        
                                                                                                            <a data-toggle="modal" data-target="#myModal" href="http://templines.rocks/html/sokolcov/auto-club/detail.html#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>VIDEO</a>
                                                                                                            <img class="img-responsive center-block" src="<?= base_url() ?>assets/images/detail/big1.jpg" alt="nissan">
                                                                                                    </li>
                                                                                                    <li class="s-relative-bx">                                        
                                                                                                            <a data-toggle="modal" data-target="#myModal" href="http://templines.rocks/html/sokolcov/auto-club/detail.html#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>VIDEO</a>
                                                                                                            <img class="img-responsive center-block" src="<?= base_url() ?>assets/images/detail/big2.jpg" alt="nissan">
                                                                                                    </li>
<!--                                                                                                    <li class="s-relative">                                        
                                                                                                            <a data-toggle="modal" data-target="#myModal" href="http://templines.rocks/html/sokolcov/auto-club/detail.html#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>VIDEO</a>
                                                                                                            <img class="img-responsive center-block" src="<?= base_url() ?>assets/images/detail/big3.jpg" alt="nissan">
                                                                                                    </li>

                                                                                                    <li class="s-relative bx-clone">                                        
                                                                                                            <a data-toggle="modal" data-target="#myModal" href="http://templines.rocks/html/sokolcov/auto-club/detail.html#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>VIDEO</a>
                                                                                                            <img class="img-responsive center-block" src="<?= base_url() ?>assets/images/detail/big1.jpg" alt="nissan">
                                                                                                    </li>-->
                                                                                                </ul>
                                                                                            </div>
<!--                                                                                            <div class="bx-controls bx-has-controls-direction">
                                                                                                <div class="bx-controls-direction">
                                                                                                    <a class="bx-prev" href="http://templines.rocks/html/sokolcov/auto-club/detail.html">Prev</a>
                                                                                                    <a class="bx-next" href="http://templines.rocks/html/sokolcov/auto-club/detail.html">Next</a>
                                                                                                </div>
                                                                                            </div>-->
                                                                                        </div>
										</div>
										<div class="col-xs-2 pagerSlider pagerVertical hidden-xs">
											<div class="bx-wrapper" style="max-width: 100%;">
                                                                                            <div class="bx-viewport" style="width: 100%; overflow: hidden; position: relative;">
                                                                                                <div class="b-detail__main-info-images-small" id="bx-pager" style="width: auto; position: relative; transition-duration: 0s; transform: translate3d(0px, 5px, 0px);">
                                                                                                        <a href="http://templines.rocks/html/sokolcov/auto-club/detail.html#" data-slide-index="7" class="b-detail__main-info-images-small-one" style="float: none; list-style: none; position: relative; width: 107px; margin-bottom: 20px;">
                                                                                                                <img class="img-responsive" src="<?= base_url() ?>assets/images/detail/small3.jpg" alt="nissan">
                                                                                                        </a>
                                                                                                        <a href="http://templines.rocks/html/sokolcov/auto-club/detail.html#" data-slide-index="0" class="b-detail__main-info-images-small-one active" style="float: none; list-style: none; position: relative; width: 107px; margin-bottom: 20px;">
                                                                                                                <img class="img-responsive" src="<?= base_url() ?>assets/images/detail/small1.jpg" alt="nissan">
                                                                                                        </a>
                                                                                                        <a href="http://templines.rocks/html/sokolcov/auto-club/detail.html#" data-slide-index="1" class="b-detail__main-info-images-small-one" style="float: none; list-style: none; position: relative; width: 107px; margin-bottom: 20px;">
                                                                                                                <img class="img-responsive" src="<?= base_url() ?>assets/images/detail/small2.jpg" alt="nissan">
                                                                                                        </a>
                                                                                                        <a href="http://templines.rocks/html/sokolcov/auto-club/detail.html#" data-slide-index="2" class="b-detail__main-info-images-small-one" style="float: none; list-style: none; position: relative; width: 107px; margin-bottom: 20px;">
                                                                                                                <img class="img-responsive" src="<?= base_url() ?>assets/images/detail/small3.jpg" alt="nissan">
                                                                                                        </a>
                                                                                                        <a href="http://templines.rocks/html/sokolcov/auto-club/detail.html#" data-slide-index="3" class="b-detail__main-info-images-small-one" style="float: none; list-style: none; position: relative; width: 107px; margin-bottom: 20px;">
                                                                                                                <img class="img-responsive" src="<?= base_url() ?>assets/images/detail/small4.jpg" alt="nissan">
                                                                                                        </a>
                                                                                                        <a href="http://templines.rocks/html/sokolcov/auto-club/detail.html#" data-slide-index="4" class="b-detail__main-info-images-small-one" style="float: none; list-style: none; position: relative; width: 107px; margin-bottom: 20px;">
                                                                                                                <img class="img-responsive" src="<?= base_url() ?>assets/images/detail/small5.jpg" alt="nissan">
                                                                                                        </a>
                                                                                                        <a href="http://templines.rocks/html/sokolcov/auto-club/detail.html#" data-slide-index="5" class="b-detail__main-info-images-small-one" style="float: none; list-style: none; position: relative; width: 107px; margin-bottom: 20px;">
                                                                                                                <img class="img-responsive" src="<?= base_url() ?>assets/images/detail/small1.jpg" alt="nissan">
                                                                                                        </a>
                                                                                                        <a href="http://templines.rocks/html/sokolcov/auto-club/detail.html#" data-slide-index="6" class="b-detail__main-info-images-small-one" style="float: none; list-style: none; position: relative; width: 107px; margin-bottom: 20px;">
                                                                                                                <img class="img-responsive" src="<?= base_url() ?>assets/images/detail/small2.jpg" alt="nissan">
                                                                                                        </a>
                                                                                                </div>
                                                                                            </div>
<!--                                                                                            <div class="bx-controls bx-has-controls-direction">
                                                                                                <div class="bx-controls-direction">
                                                                                                    <a class="bx-prev disabled" href="http://templines.rocks/html/sokolcov/auto-club/detail.html"><span class="fa fa-angle-up"></span></a>
                                                                                                    <a class="bx-next" href="http://templines.rocks/html/sokolcov/auto-club/detail.html"><span class="fa fa-angle-down"></span></a>
                                                                                                </div>
                                                                                            </div>-->
                                                                                        </div>
										</div>
									</div>
								</div>
<!--								<div class="b-detail__main-info-characteristics wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
									<div class="b-detail__main-info-characteristics-one">
										<div class="b-detail__main-info-characteristics-one-top">
											<div><span class="fa fa-car"></span></div>
											<p>Brand New</p>
										</div>
										<div class="b-detail__main-info-characteristics-one-bottom">
											Status
										</div>
									</div>
									<div class="b-detail__main-info-characteristics-one">
										<div class="b-detail__main-info-characteristics-one-top">
											<div><span class="fa fa-trophy"></span></div>
											<p>5,000KM</p>
										</div>
										<div class="b-detail__main-info-characteristics-one-bottom">
											Warrenty
										</div>
									</div>
									<div class="b-detail__main-info-characteristics-one">
										<div class="b-detail__main-info-characteristics-one-top">
											<div><span class="fa fa-at"></span></div>
											<p>Auto</p>
										</div>
										<div class="b-detail__main-info-characteristics-one-bottom">
											Transmission
										</div>
									</div>
									<div class="b-detail__main-info-characteristics-one">
										<div class="b-detail__main-info-characteristics-one-top">
											<div><span class="fa fa-car"></span></div>
											<p>FWD</p>
										</div>
										<div class="b-detail__main-info-characteristics-one-bottom">
											Drivetrain
										</div>
									</div>
									<div class="b-detail__main-info-characteristics-one">
										<div class="b-detail__main-info-characteristics-one-top">
											<div><span class="fa fa-user"></span></div>
											<p>5</p>
										</div>
										<div class="b-detail__main-info-characteristics-one-bottom">
											Passangers
										</div>
									</div>
									<div class="b-detail__main-info-characteristics-one">
										<div class="b-detail__main-info-characteristics-one-top">
											<div><span class="fa fa-fire-extinguisher"></span></div>
											<p>10.8L</p>
										</div>
										<div class="b-detail__main-info-characteristics-one-bottom">
											In City
										</div>
									</div>
									<div class="b-detail__main-info-characteristics-one">
										<div class="b-detail__main-info-characteristics-one-top">
											<div><span class="fa fa-fire-extinguisher"></span></div>
											<p>7.5L</p>
										</div>
										<div class="b-detail__main-info-characteristics-one-bottom">
											On Highway
										</div>
									</div>
								</div>-->
								<div class="b-detail__main-info-text wow zoomInUp" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none; padding: 15px;">
									<div class="b-detail__main-aside-about-form-links">
										<a href="#" class="j-tab m-active s-lineDownCenter" data-to="#info1">TITLE GALLERY 1</a>
										<a href="#" class="j-tab" data-to="#info2">TITLE GALLERY 2</a>
										<a href="#" class="j-tab" data-to="#info3">TITLE GALLERY 3</a>
										<a href="#" class="j-tab" data-to="#info4">TITLE GALLERY 4</a>
									</div>
									<div id="info1">
										<p>Paragraph 1.1</p>
										<p>Paragraph 1.2</p>
										<p>Paragraph 1.3</p>
									</div>
									<div id="info2">
										<p>Paragraph 2.1</p>
										<p>Paragraph 2.2</p>
										<p>Paragraph 2.3</p>
									</div>
									<div id="info3">
										<p>Paragraph 3.1</p>
										<p>Paragraph 3.2</p>
										<p>Paragraph 3.3</p>
									</div>
									<div id="info4">
										<p>Paragraph 4.1</p>
										<p>Paragraph 4.2</p>
										<p>Paragraph 4.3</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-xs-12">
							<aside class="b-detail__main-aside">
								<div class="b-detail__main-aside-desc wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
									<h2 class="s-titleDet">Detail</h2>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Tanggal</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?= masking_date($eventDate)?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Peserta</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?= $eventParticipant?> Anggota</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Titik Kumpul</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?= $eventMeetPoint?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Lokasi</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?= $eventLocation?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Jarak</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?= $eventDistance?> Km</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Durasi</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?= $eventDuration?></p>
										</div>
									</div>
								</div>
                                                                <div class="b-detail__main-info-extra wow zoomInUp" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">
									<h2 class="s-titleDet">FASILITAS EXTRA</h2>
									<div class="row">
										<div class="col-xs-6">
											<ul>
                                                                                            <?php 
                                                                                            $idx = 0; 
                                                                                            foreach($eventFacilitate as $data){
                                                                                                if($idx < 8){
                                                                                            ?>
												<li><span class="fa fa-check"></span><?= $data["name"]?></li>
                                                                                            <?php }$idx++;}?>
											</ul>
										</div>
										<div class="col-xs-6">
											<ul>
                                                                                            <?php 
                                                                                            $idx = 0; 
                                                                                            foreach($eventFacilitate as $data){
                                                                                                if($idx >= 8){
                                                                                            ?>
												<li><span class="fa fa-check"></span><?= $data["name"]?></li>
                                                                                            <?php }$idx++;}?>
											</ul>
										</div>
									</div>
								</div>
<!--								<div class="b-detail__main-aside-about wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
									<h2 class="s-titleDet">INQUIRE ABOUT THIS VEHICLE</h2>
									<div class="b-detail__main-aside-about-call">
										<span class="fa fa-phone"></span>
										<div>1-888-378-4027</div>
										<p>Call the seller 24/7 and they would help you.</p>
									</div>
									<div class="b-detail__main-aside-about-seller">
										<p>Seller Info: <span>NissanCarDealer</span></p>
									</div>
									<div class="b-detail__main-aside-about-form">
										<div class="b-detail__main-aside-about-form-links">
											<a href="http://templines.rocks/html/sokolcov/auto-club/detail.html#" class="j-tab m-active s-lineDownCenter" data-to="#form1">GENERAL INQUIRY</a>
											<a href="http://templines.rocks/html/sokolcov/auto-club/detail.html#" class="j-tab" data-to="#form2">SCHEDULE TEST DRIVE</a>
										</div>
										<form id="form1" action="http://templines.rocks/" method="post">
											<input type="text" placeholder="YOUR NAME" value="" name="name">
											<input type="email" placeholder="EMAIL ADDRESS" value="" name="email">
											<input type="tel" placeholder="PHONE NO." value="" name="name">
											<textarea name="text" placeholder="message"></textarea>
											<div><input type="checkbox" name="one" value=""><label>Send me a copy of this message</label></div>
											<div><input type="checkbox" name="two" value=""><label>Send me a copy of this message</label></div>
											<button type="submit" class="btn m-btn">SEND MESSAGE<span class="fa fa-angle-right"></span></button>
										</form>
										<form id="form2" action="http://templines.rocks/" method="post">
											<input type="text" placeholder="YOUR NAME" value="" name="name">
											<textarea name="text" placeholder="message"></textarea>
											<div><input type="checkbox" name="one" value=""><label>Send me a copy of this message</label></div>
											<div><input type="checkbox" name="two" value=""><label>Send me a copy of this message</label></div>
											<button type="submit" class="btn m-btn">SEND MESSAGE<span class="fa fa-angle-right"></span></button>
										</form>
									</div>
								</div>
								<div class="b-detail__main-aside-payment wow zoomInUp" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">
									<h2 class="s-titleDet">CAR PAYMENT CALCULATOR</h2>
									<div class="b-detail__main-aside-payment-form">
										<form action="http://templines.rocks/" method="post">
											<input type="text" placeholder="TOTAL VALUE/LOAN AMOUNT" value="" name="name">
											<input type="text" placeholder="DOWN PAYMENT" value="" name="name">
											<div class="s-relative">
												<select name="select" class="m-select">
													<option value="">LOAN TERM IN MONTHS</option>
												</select>
												<span class="fa fa-caret-down"></span>
											</div>
											<input type="text" placeholder="INTEREST RATE IN %" value="" name="name">
											<button type="submit" class="btn m-btn">ESTIMATE PAYMENT<span class="fa fa-angle-right"></span></button>
										</form>
									</div>
									<div class="b-detail__main-aside-about-call">
										<span class="fa fa-calculator"></span>
										<div>$250 <p>PER MONTH</p></div>
										<p>Total Number of Payments: <span>50</span></p>
									</div>
								</div>-->
							</aside>
						</div>
					</div>
				</div>
			</div>
		</section><!--b-detail-->

		<section class="b-related m-home">
			<div class="container">
				<h5 class="s-titleBg wow zoomInUp" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">Perlu Anda Ketahui</h5><br>
				<h2 class="s-title wow zoomInUp" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">Kegiatan Menarik Lainnya</h2>
				<div class="row">
                                    <?php foreach($eventMore as $data){?>
					<div class="col-md-3 col-xs-6">
						<div class="b-auto__main-item wow zoomInLeft" data-wow-delay="0.5s" style="height: 336px; visibility: hidden; animation-delay: 0.5s; animation-name: none;">
							<img class="img-responsive center-block" src="<?=$imagePath.$data["type"]."/".$data["photo"]?>" alt="Kegiatan Lainnya">
							<div class="b-world__item-val s-lineDownLeft">
								<span class="b-world__item-val-title">Lokasi : <span><?= $data["location"]?></span></span>
							</div>
							<h2><a href="<?= base_url()."event/detail/".$data["id"] ?>"><?= $data["name"]?></a></h2>
							<div class="b-auto__main-item-info">
								<span class="m-price">
                                                                    <?= $data["participants"]?> Anggota
								</span>
								<span class="m-number">
                                                                    <span class="fa fa-calendar"></span><?= masking_date($data["date"])?>
								</span>
							</div>
<!--							<div class="b-featured__item-links m-auto">
								<a href="http://templines.rocks/html/sokolcov/auto-club/detail.html#">Used</a>
								<a href="http://templines.rocks/html/sokolcov/auto-club/detail.html#">2014</a>
								<a href="http://templines.rocks/html/sokolcov/auto-club/detail.html#">Manual</a>
								<a href="http://templines.rocks/html/sokolcov/auto-club/detail.html#">Orange</a>
								<a href="http://templines.rocks/html/sokolcov/auto-club/detail.html#">Petrol</a>
							</div>-->
						</div>
					</div>
                                    <?php }?>
				</div>
			</div>
		</section><!--"b-related-->

		<section class="b-brands s-shadow">
			<div class="container">
				<h5 class="s-titleBg wow zoomInUp" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;"><?=$eventName?></h5><br>
				<h2 class="s-title wow zoomInUp" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">DIDUKUNG OLEH</h2>
				<div class="">
					<div class="b-brands__brand wow rotateIn" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">
						<img src="<?= base_url() ?>assets/images/detail/bmwLogo.png" alt="brand">
					</div>
					<div class="b-brands__brand wow rotateIn" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">
						<img src="<?= base_url() ?>assets/images/detail/ferrariLogo.png" alt="brand">
					</div>
					<div class="b-brands__brand wow rotateIn" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">
						<img src="<?= base_url() ?>assets/images/detail/volvo.png" alt="brand">
					</div>
					<div class="b-brands__brand wow rotateIn" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">
						<img src="<?= base_url() ?>assets/images/detail/mercLogo.png" alt="brand">
					</div>
					<div class="b-brands__brand wow rotateIn" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">
						<img src="<?= base_url() ?>assets/images/detail/audiLogo.png" alt="brand">
					</div>
					<div class="b-brands__brand wow rotateIn" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">
						<img src="<?= base_url() ?>assets/images/detail/honda.png" alt="brand">
					</div>
					<div class="b-brands__brand wow rotateIn" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">
						<img src="<?= base_url() ?>assets/images/detail/peugeot.png" alt="brand">
					</div>
				</div>
			</div>
		</section><!--b-brands-->