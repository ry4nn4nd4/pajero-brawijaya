<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCNDRoez-toGCw0jEVCg7wpMg5V0tyGlwI"></script>
<section class="b-pageHeader">
        <div class="container">
                <h1 class="wow zoomInLeft" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInLeft;">Contact Us</h1>
                <div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInRight;">
                        <h3>Get In Touch With Us Now</h3>
                </div>
        </div>
</section><!--b-pageHeader-->

<div class="b-breadCumbs s-shadow wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
        <div class="container">
                <a href="<?= base_url() ?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span>
                <a href="<?= base_url() ?>contact" class="b-breadCumbs__page m-active">Contact Us</a>
        </div>
</div><!--b-breadCumbs-->

<div class="b-map wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
    <div id="map"></div>
    <script type="text/javascript"> 
        function init_map() {
            var myOptions = {zoom: 14, center: new google.maps.LatLng(-7.360321, 112.7408992), mapTypeId: google.maps.MapTypeId.ROADMAP};
            map = new google.maps.Map(document.getElementById("map"), myOptions);
            marker = new google.maps.Marker({map: map, position: new google.maps.LatLng(-7.360321, 112.7408992)});
            infowindow = new google.maps.InfoWindow({content: "<b>Kantor Pusat Jadipergi</b><br/>Jalan Delta Raya Utara No. 48<br/>61256 Sidoarjo"});
            google.maps.event.addListener(marker, "click", function () {
                infowindow.open(map, marker);
            });
            infowindow.open(map, marker);
        }
        google.maps.event.addDomListener(window, 'load', init_map);
    </script>
</div><!--b-map-->

<section class="b-contacts s-shadow">
        <div class="container">
                <div class="row">
                        <div class="col-xs-6">
                                <div class="b-contacts__form">
                                        <header class="b-contacts__form-header s-lineDownLeft wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                <h2 class="s-titleDet">Contact Form</h2> 
                                        </header>
                                        <p class="wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">Enter your comments through the form below, and our customer service professionals will contact you as soon as possible.</p>
                                        <div id="success"></div>
                                        <form id="contactForm" novalidate="" class="s-form wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                <div class="s-relative">
                                                        <select name="user-topic" id="user-topic" class="m-select">
                                                                <option value="Not select">SELECT A TOPIC</option>
                                                                <option value="Topic 1">TOPIC 1</option>
                                                                <option value="Topic 2">TOPIC 2</option>
                                                                <option value="Topic 3">TOPIC 3</option>
                                                                <option value="Topic 4">TOPIC 4</option>
                                                        </select>
                                                        <span class="fa fa-caret-down"></span>
                                                </div>
                                                <input type="text" placeholder="YOUR NAME" value="" name="user-name" id="user-name">
                                                <input type="text" placeholder="EMAIL ADDRESS %" value="" name="user-email" id="user-email">
                                                <input type="text" placeholder="PHONE NO." value="" name="user-phone" id="user-phone">
                                                <textarea id="user-message" name="user-message" placeholder="COMMENT/SUGGESTIONS/FEEDBACK"></textarea>
                                                <button type="submit" class="btn m-btn">SUBMIT NOW<span class="fa fa-angle-right"></span></button>
                                        </form>
                                </div>
                        </div>
                        <div class="col-xs-6">
                                <div class="b-contacts__address">
                                        <div class="b-contacts__address-hours">
                                                <h2 class="s-titleDet wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">opening hours</h2>
                                                <div class="b-contacts__address-hours-main wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                        <div class="row">
                                                                <div class="col-md-6 col-xs-12">
                                                                        <h5>Sales Department</h5>
                                                                        <p>Mon-Sat : 8:00am - 5:00pm <br>Sunday is closed</p>
                                                                </div>
                                                                <div class="col-md-6 col-xs-12">
                                                                        <h5>Service Department</h5>
                                                                        <p>Mon-Sat : 8:00am - 5:00pm <br>Sunday is closed</p>
                                                                </div>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="b-contacts__address-info">
                                                <h2 class="s-titleDet wow zoomInUp" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">opening hours</h2>
                                                <address class="b-contacts__address-info-main wow zoomInUp" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">
                                                        <div class="b-contacts__address-info-main-item">
                                                                <span class="fa fa-home"></span>
                                                                ADDRESS
                                                                <p>202 W 7th St, Suite 233 Los Angeles, California 90014 USA</p>
                                                        </div>
                                                        <div class="b-contacts__address-info-main-item">
                                                                <div class="row">
                                                                        <div class="col-lg-3 col-md-4 col-xs-12">
                                                                                <span class="fa fa-phone"></span>
                                                                                PHONE
                                                                        </div>
                                                                        <div class="col-lg-9 col-md-8 col-xs-12">
                                                                                <em>1-800- 624-5462</em>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                        <div class="b-contacts__address-info-main-item">
                                                                <div class="row">
                                                                        <div class="col-lg-3 col-md-4 col-xs-12">
                                                                                <span class="fa fa-fax"></span>
                                                                                FAX
                                                                        </div>
                                                                        <div class="col-lg-9 col-md-8 col-xs-12">
                                                                                <em>1-800- 624-5462</em>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                        <div class="b-contacts__address-info-main-item">
                                                                <div class="row">
                                                                        <div class="col-lg-3 col-md-4 col-xs-12">
                                                                                <span class="fa fa-envelope"></span>
                                                                                EMAIL
                                                                        </div>
                                                                        <div class="col-lg-9 col-md-8 col-xs-12">
                                                                                <em>info@domain.com</em>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                </address>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</section><!--b-contacts-->

<script type="text/javascript">
    $(document).ready(function(){
        $('<script/>',{type:'text/javascript', src:'<?= base_url() ?>assets/js/jqBootstrapValidation.js'}).appendTo('head');
        $('<script/>',{type:'text/javascript', src:'<?= base_url() ?>assets/js/contact_me.js'}).appendTo('head');
     }); 
 </script>