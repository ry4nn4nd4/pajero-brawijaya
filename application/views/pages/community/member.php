<?php
    function maskingName($name){
        if(strlen($name) > 25){
            $name = substr($name,0,22) . " ...";
        }
        return $name;
    }
?>
<section class="b-pageHeader">
        <div class="container">
                <h1 class="wow zoomInLeft" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInLeft;">Anggota</h1>
                <div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInRight;">
                        <h3>Armada Brawijaya</h3>
                </div>
        </div>
</section><!--b-pageHeader-->

<div class="b-breadCumbs s-shadow">
        <div class="container wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                <a href="<?= base_url() ?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span>
                <a href="<?= base_url() ?>/community/member" class="b-breadCumbs__page m-active">Anggota</a>
        </div>
</div><!--b-breadCumbs-->

<!--b-infoBar
<div class="b-infoBar">
        <div class="container">
                <div class="row">
                        <div class="col-lg-4 col-xs-12">
                                <div class="b-infoBar__compare wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                        <div class="dropdown">
                                                <a href="http://templines.rocks/html/sokolcov/auto-club/listTableTwo.html#" class="dropdown-toggle b-infoBar__compare-item" data-toggle="dropdown"><span class="fa fa-clock-o"></span>RECENTLY VIEWED<span class="fa fa-caret-down"></span></a>
                                                <ul class="dropdown-menu">
                                                        <li><a href="http://templines.rocks/html/sokolcov/auto-club/listTableTwo.html#">Item</a></li>
                                                        <li><a href="http://templines.rocks/html/sokolcov/auto-club/listTableTwo.html#">Item</a></li>
                                                        <li><a href="http://templines.rocks/html/sokolcov/auto-club/listTableTwo.html#">Item</a></li>
                                                        <li><a href="http://templines.rocks/html/sokolcov/auto-club/listTableTwo.html#">Item</a></li>
                                                </ul>
                                        </div>
                                        <a href="http://templines.rocks/html/sokolcov/auto-club/listTableTwo.html#" class="b-infoBar__compare-item"><span class="fa fa-compress"></span>COMPARE VEHICLES: 2</a>
                                </div>
                        </div>
                        <div class="col-lg-8 col-xs-12">
                                <div class="b-infoBar__select wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                        <form method="post" action="http://templines.rocks/">
                                                <div class="b-infoBar__select-one">
                                                        <span class="b-infoBar__select-one-title">SELECT VIEW</span>
                                                        <a href="http://templines.rocks/html/sokolcov/auto-club/listings.html" class="m-list"><span class="fa fa-list"></span></a>
                                                        <a href="http://templines.rocks/html/sokolcov/auto-club/listTableTwo.html" class="m-table m-active"><span class="fa fa-table"></span></a>
                                                </div>
                                                <div class="b-infoBar__select-one">
                                                        <span class="b-infoBar__select-one-title">SHOW ON PAGE</span>
                                                        <select name="select1" class="m-select">
                                                                <option value="" selected="">10 items</option>
                                                        </select>
                                                        <span class="fa fa-caret-down"></span>
                                                </div>
                                                <div class="b-infoBar__select-one">
                                                        <span class="b-infoBar__select-one-title">SORT BY</span>
                                                        <select name="select2" class="m-select">
                                                                <option value="" selected="">Last Added</option>
                                                        </select>
                                                        <span class="fa fa-caret-down"></span>
                                                </div>
                                        </form>
                                </div>
                        </div>
                </div>
        </div>
</div>
b-infoBar-->

<div class="b-items">
        <div class="container">
                <div class="row">
<!--                        <div class="col-lg-3 col-sm-4 col-xs-12">
                                <aside class="b-items__aside">
                                        <h2 class="s-title wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">REFINE YOUR SEARCH</h2>
                                        <div class="b-items__aside-main wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                <form>
                                                        <div class="b-items__aside-main-body">
                                                                <div class="b-items__aside-main-body-item">
                                                                        <label>SELECT A MAKE</label>
                                                                        <div>
                                                                                <select name="select1" class="m-select">
                                                                                        <option value="" selected="">Any Make</option>
                                                                                </select>
                                                                                <span class="fa fa-caret-down"></span>
                                                                        </div>
                                                                </div>
                                                                <div class="b-items__aside-main-body-item">
                                                                        <label>SELECT A MODEL</label>
                                                                        <div>
                                                                                <select name="select1" class="m-select">
                                                                                        <option value="" selected="">Any Make</option>
                                                                                </select>
                                                                                <span class="fa fa-caret-down"></span>
                                                                        </div>
                                                                </div>
                                                                <div class="b-items__aside-main-body-item">
                                                                        <label>PRICE RANGE</label>
                                                                        <div class="slider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false"><div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 0%; width: 100%;"></div><a class="ui-slider-handle ui-state-default ui-corner-all" href="http://templines.rocks/html/sokolcov/auto-club/listTableTwo.html#" style="left: 0%;"><span class="min">100</span></a><a class="ui-slider-handle ui-state-default ui-corner-all" href="http://templines.rocks/html/sokolcov/auto-club/listTableTwo.html#" style="left: 100%;"><span class="max">1000</span></a></div>
                                                                        <input type="hidden" name="min" value="100" class="j-min">
                                                                        <input type="hidden" name="max" value="1000" class="j-max">
                                                                </div>
                                                                <div class="b-items__aside-main-body-item">
                                                                        <label>VEHICLE TYPE</label>
                                                                        <div>
                                                                                <select name="select1" class="m-select">
                                                                                        <option value="" selected="">Any Type</option>
                                                                                </select>
                                                                                <span class="fa fa-caret-down"></span>
                                                                        </div>
                                                                </div>
                                                                <div class="b-items__aside-main-body-item">
                                                                        <label>VEHICLE STATUS</label>
                                                                        <div>
                                                                                <select name="select1" class="m-select">
                                                                                        <option value="" selected="">Any Status</option>
                                                                                </select>
                                                                                <span class="fa fa-caret-down"></span>
                                                                        </div>
                                                                </div>
                                                                <div class="b-items__aside-main-body-item">
                                                                        <label>FUEL TYPE</label>
                                                                        <div>
                                                                                <select name="select1" class="m-select">
                                                                                        <option value="" selected="">All Fuel Types</option>
                                                                                </select>
                                                                                <span class="fa fa-caret-down"></span>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                        <footer class="b-items__aside-main-footer">
                                                                <button type="submit" class="btn m-btn">FILTER VEHICLES<span class="fa fa-angle-right"></span></button><br>
                                                                <a href="http://templines.rocks/html/sokolcov/auto-club/listTableTwo.html">RESET ALL FILTERS</a>
                                                        </footer>
                                                </form>
                                        </div>
                                        <div class="b-items__aside-sell wow zoomInUp" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">
                                                <div class="b-items__aside-sell-img">
                                                        <h3>SELL YOUR CAR</h3>
                                                </div>
                                                <div class="b-items__aside-sell-info">
                                                        <p>
                                                                Nam tellus enimds eleifend dignis lsim
                                                                biben edum tristique sed metus fusce
                                                                Maecenas lobortis.
                                                        </p>
                                                        <a href="http://templines.rocks/html/sokolcov/auto-club/submit1.html" class="btn m-btn">REGISTER NOW<span class="fa fa-angle-right"></span></a>
                                                </div>
                                        </div>
                                </aside>
                        </div>-->
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                                <form class="b-blog__aside-search wow zoomInUp" data-wow-delay="0.3s" action="http://localhost/pajero-brawijaya/" method="post" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">
                                        <div>
                                                <input type="text" name="search" value="" placeholder="Search">
                                                <button type="submit"><span class="fa fa-search"></span></button>
                                        </div>        
                                </form>
                                <div class="row m-border">
                                        <?php foreach($memberList as $data){?>
                                        <div class="col-lg-3 col-md-6 col-xs-12 wow zoomInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInUp;">
                                                <div class="b-items__cell">
                                                        <div class="b-items__cars-one-img">
                                                                <img class="img-responsive" src="<?=$imagePath.$data["member_photo"]?>" alt="<?=$imagePath.$data["member_photo"]?>">
<!--                                                                <a href="http://templines.rocks/html/sokolcov/auto-club/listTableTwo.html#" data-toggle="modal" data-target="#myModal" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>VIDEO</a>
                                                                <span class="b-items__cars-one-img-type m-premium">PREMIUM</span>
                                                                <form action="http://templines.rocks/" method="post">
                                                                        <input type="checkbox" name="check1" id="check1">
                                                                        <label for="check1" class="b-items__cars-one-img-check"><span class="fa fa-check"></span></label>
                                                                </form>-->
                                                        </div>
                                                        <div class="b-items__cell-info">
                                                                <div class="s-lineDownLeft b-items__cell-info-title">
                                                                        <h2 class=""><?=maskingName($data["member_name"])?></h2>
                                                                </div>
                                                                <p>Informasi Mobil</p>
                                                                <div>
                                                                        <div class="row m-smallPadding">
                                                                                <div class="col-xs-5">
                                                                                        <span class="b-items__cars-one-info-title">Model:</span>
                                                                                        <span class="b-items__cars-one-info-title">Type:</span>
                                                                                        <span class="b-items__cars-one-info-title">Tahun:</span>
                                                                                        <span class="b-items__cars-one-info-title">Warna:</span>
                                                                                </div>
                                                                                <div class="col-xs-7">
                                                                                        <span class="b-items__cars-one-info-value"><?=$data["model"]?></span>
                                                                                        <span class="b-items__cars-one-info-value"><?=$data["car_type"]?></span>
                                                                                        <span class="b-items__cars-one-info-value"><?=$data["year"]?></span>
                                                                                        <span class="b-items__cars-one-info-value"><?=$data["color"]?></span>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <h5 class="b-items__cell-info-price"><span></span><?= ($data["police_number"] == "" ? "-" : $data["police_number"])?></h5>
                                                        </div>
                                                </div>
                                        </div>
                                        <?php }?>
                                </div>
                                <div class="b-items__pagination">
                                        <div class="b-items__pagination-main wow zoomInUp" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">
                                                <a data-toggle="modal" data-target="#myModal" href="http://templines.rocks/html/sokolcov/auto-club/listTableTwo.html#" class="m-left"><span class="fa fa-angle-left"></span></a>
                                                <span class="m-active"><a href="http://templines.rocks/html/sokolcov/auto-club/listTableTwo.html#">1</a></span>
                                                <span><a href="http://templines.rocks/html/sokolcov/auto-club/listTableTwo.html#">2</a></span>
                                                <span><a href="http://templines.rocks/html/sokolcov/auto-club/listTableTwo.html#">3</a></span>
                                                <span><a href="http://templines.rocks/html/sokolcov/auto-club/listTableTwo.html#">4</a></span>
                                                <a href="http://templines.rocks/html/sokolcov/auto-club/listTableTwo.html#" class="m-right"><span class="fa fa-angle-right"></span></a>    
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</div><!--b-items-->