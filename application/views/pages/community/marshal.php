
<section class="b-pageHeader">
        <div class="container">
                <h1 class="wow zoomInLeft" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInLeft;"><?=$title?></h1>
                <div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomInRight;">
                        <h3><?= $subtitle?></h3>
                </div>
        </div>
</section><!--b-pageHeader-->
<section class="b-compare s-shadow">
        <div class="container">
                <div class="b-compare__images">
                        <div class="row">
                            <?php
                                $count=0;
                                foreach($chapter as $data){
                                $count++;
                            ?>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                        <div class="b-compare__images-item s-lineDownCenter wow zoomInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">
                                                <h3><?=$data["position"]?> Chapter</h3>
                                                <img class="img-responsive center-block" src="<?=$imagePath.$data["photo"]?>" alt="<?=$data["photo"]?>">
                                                <div class="b-compare__images-item-price m-right">
                                                    <?= ($count<4) ? '<div class="b-compare__images-item-price-vs"> - </div>' : '' ?>
                                                    <?=$data["member_name"]?>
                                                </div>
                                        </div>
                                </div>
                            <?php }?>
                        </div>
                </div>
                <?php foreach($sector as $data){?>
                <div class="b-compare__block wow zoomInUp" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">
                        <div class="b-compare__block-title s-whiteShadow">
                                <h3 class="s-titleDet"><?=$data["name"]?></h3>
                                <a class="j-more" href="<?= base_url() ?>community/marshal"><span class="fa fa-angle-left"></span></a>
                        </div>
                        <div class="b-compare__block-inside j-inside">
                                <div class="row">
                                        <div class="col-xs-6">
                                                <div class="b-compare__block-inside-title">
                                                        Ketua
                                                </div>
                                        </div>
                                        <?php foreach($data["ketua"] as $dataKetua){?>
                                        <div class="col-xs-6">
                                                <div class="b-compare__block-inside-value">
                                                        <div class="b-compare__images-item s-lineDownCenter wow zoomInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">
                                                                <img class="img-responsive center-block" src="<?=$imagePath.$dataKetua["photo"]?>" alt="<?=$dataKetua["photo"]?>">
                                                                <div class="b-compare__images-item-price m-right m-left"><?=$dataKetua["member_name"]?></div>
                                                        </div>
                                                </div>
                                        </div>
                                        <?php }?>
                                </div>
                                <div class="row">
                                        <div class="col-xs-3">
                                                <div class="b-compare__block-inside-title">
                                                        Anggota
                                                </div>
                                        </div>
                                        <?php foreach($data["anggota"] as $dataAnggota){?>
                                        <div class="col-xs-3">
                                                <div class="b-compare__block-inside-value">
                                                        <div class="b-compare__images-item s-lineDownCenter wow zoomInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">
                                                                <img class="img-responsive center-block" src="<?=$imagePath.$dataAnggota["photo"]?>" alt="<?=$dataAnggota["photo"]?>">
                                                                <div class="b-compare__images-item-price m-left"><?=$dataAnggota["member_name"]?></div>
                                                        </div>
                                                </div>
                                        </div>
                                        <?php }?>
                                </div>
                        </div>
                </div>
                <?php }?>
                <div class="b-compare__links wow zoomInUp" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">
                        <div class="row">
                                <div class="col-sm-3 col-xs-12 col-sm-offset-3">
                                        <a href="<?= base_url() ?>community/member" class="btn m-btn">Lihat Semua Anggota<span class="fa fa-angle-right"></span></a>
                                </div>
                        </div>
                </div>
        </div>
</section><!--b-compare-->