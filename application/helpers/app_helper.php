<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('masking_date')){
    function masking_date($date){
        $time = strtotime($date);
        return date('d M Y',$time);
    }
}