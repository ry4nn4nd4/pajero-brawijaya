<?php

class Member_model extends My_Model {

    public function __construct() {
        parent::__construct();
    }

    function sign_in($username, $pin) {
        $query = "SELECT * FROM member WHERE active = 1 AND username = '".$username."' AND pin = md5('".$pin."')";
        $result = $this->db->query($query);
        return $result->result_array();
    }
        

    function get_member_data($memberId) {
        $query = "SELECT *,m.name AS member_name,m.address AS member_address,m.photo AS member_photo,"
                . "c.name AS car_name,c.type AS car_type,c.photo AS car_photo,"
                . "w.name AS company_name, w.address AS company_address "
                . "FROM member m "
                . "LEFT JOIN car c ON m.id = c.member_id "
                . "LEFT JOIN company w ON m.id = w.member_id "
                . "WHERE m.id = " . $memberId;
        $result = $this->db->query($query);
        return $result->result_array();
    }

    function register($data = array()) {
        $this->db->trans_start();
        
        //Insert Data Member
        $member = json_decode($data["member"], true);
        $this->db->insert('member', $member);
        $memberId = $this->db->insert_id();
                
        //Insert Data Car
        $car = json_decode($data["car"], true);
        $car["member_id"] = $memberId;
        $this->db->insert('car', $car);
        
        //Insert Data Company
        $company = json_decode($data["company"], true);
        $company["member_id"] = $memberId;
        $this->db->insert('company', $company);
        
        $this->db->trans_complete();
        
        return $memberId;
    }
    
     function get_sector_data() {
        $query = "SELECT * FROM structure_sector WHERE sequence > 0 ORDER BY sequence";
        $result = $this->db->query($query);
        return $result->result_array();
    }
    
    function get_marshal_list() {
        $query = "SELECT *, m.name AS member_name, s.name AS sector_name FROM structure_organitation o
            LEFT JOIN member m ON o.member_id = m.id
            LEFT JOIN structure_sector s ON o.sector_id = s.id";
        $result = $this->db->query($query);
        return $result->result_array();
    }
    function get_member_list() {
        $query = "SELECT *,m.name AS member_name,m.address AS member_address,m.photo AS member_photo,"
                . "c.name AS car_name,c.type AS car_type,c.photo AS car_photo,"
                . "w.name AS company_name, w.address AS company_address "
                . "FROM member m "
                . "LEFT JOIN car c ON m.id = c.member_id "
                . "LEFT JOIN company w ON m.id = w.member_id "
                . "WHERE m.structure_position = 'anggota'";
        $result = $this->db->query($query);
        return $result->result_array();
    }
    
    function updateMemberPhoto($photo,$memberId){
        $query = "UPDATE member SET photo = '".$photo."' WHERE id = ". $memberId;
        $this->db->query($query); 
        return $this->db->affected_rows() > 0;
    }
    
    function updateCarPhoto($photo,$memberId){
        $query = "UPDATE car SET photo = '".$photo."' WHERE member_id = ". $memberId;
        $this->db->query($query); 
        return $this->db->affected_rows() > 0;
    }
    
    function updateMemberCredential($username,$pin,$memberId){
        $query = "UPDATE member SET username = '".$username."', pin = md5('".$pin."') WHERE id = ". $memberId;
        $this->db->query($query); 
        return $this->db->affected_rows() > 0;
    }
    
    function updateMemberPin($pin,$memberId){
        $query = "UPDATE member SET pin = md5('".$pin."') WHERE id = ". $memberId;
        $this->db->query($query); 
        return $this->db->affected_rows() > 0;
    }
    
    function updateProfile($userId, $data = array()) {
        $this->db->trans_start();
        
        //Update Data Member
        $memberData = array(
            "call_name" => $data["member_call_name"],
            "activity_status" => $data["member_activity_status"],
            "name" => $data["member_name"],
            "phone" => $data["member_phone"],
            "email" => $data["member_email"],
            "address" => $data["member_address"],
            "id_card_number" => $data["member_id_card_number"]
        );
        $this->db->where("id", $userId);
        $this->db->update("member", $memberData);
                
        //Update Data Car
        $carData = array(
            "type" => $data["car_type"],
            "year" => $data["car_year"],
            "color" => $data["car_color"],
            "police_number" => $data["car_police_number"],
            "machine_number" => $data["car_machine_number"],
            "body_number" => $data["car_body_number"]
        );
        $this->db->where("id", $userId);
        $this->db->update("car", $carData);
        
        //Update Data Company
        $companyData = array(
            "work" => $data["company_work"],
            "type" => $data["company_type"],
            "name" => $data["company_name"],
            "address" => $data["company_address"]
        );
        $this->db->where("id", $userId);
        $this->db->update("company", $companyData);
        
        $this->db->trans_complete();
        
        return $userId;
    }
    
    function isExistUsername($username, $id) {
        $query = "SELECT id FROM member WHERE username = '".$username."' AND id != ".$id;
        $result = $this->db->query($query);
        return $result->result_array();
    }
}
