<?php

class Event_model extends My_Model {

    public function __construct() {
        parent::__construct();
    }

    function get_event_list($type) {
        $query = "SELECT * FROM event WHERE type = '" . $type . "'";
        $result = $this->db->query($query);
        return $result->result_array();
    }
    
    function get_event_data($eventId) {
        $query = "SELECT *, e.title AS event_title, e.description AS event_description, e.photo AS event_photo, "
                . "g.title AS gallery_title, g.description AS gallery_description, g.photo AS gallery_photo "
                . "FROM event e "
                . "LEFT JOIN event_gallery g ON g.event_id = e.id "
                . "WHERE e.id = " . $eventId;
        $result = $this->db->query($query);
        return $result->result_array();
    }
    
    function get_event_facilitate($eventId) {
        $query = "SELECT * FROM event_facilitate WHERE event_id IN(0," . $eventId . ") LIMIT 16";
        $result = $this->db->query($query);
        return $result->result_array();
    }
    
    function get_event_more($eventId) {
        $query = "SELECT * FROM event WHERE id != '" . $eventId . "' LIMIT 4";
        $result = $this->db->query($query);
        return $result->result_array();
    }
    
}
