<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {

    function __construct() {	
        parent::__construct();        
    }
    
    public function index()
    {
        $data = array();
        $this->load->view("template/header");
        $this->load->view("pages/home", $data);
        $this->load->view("template/footer");
    }
    
    public function detail()
    {
        $data = array();
        $this->load->view("template/header");
        $this->load->view("pages/detail", $data);
        $this->load->view("template/footer");
    }
}
