<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

    var $mode;
    function __construct() {	
        parent::__construct();       
        $this->load->model("member_model");
        $this->mode = "test";
    }
    
    public function register1()
    {
        $data = array(
            "mode" => $this->mode
        );
        $this->load->view("template/header");
        $this->load->view("pages/account/header_register");
        $this->load->view("pages/account/register1", $data);
        $this->load->view("template/footer");
    }
    
    public function register2()
    {
        $data = array(
            "mode" => $this->mode,
            "car" => ""
        );
        
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $data["car"] = json_encode($_POST);
        }
        
        $this->load->view("template/header");
        $this->load->view("pages/account/header_register");
        $this->load->view("pages/account/register2", $data);
        $this->load->view("template/footer");
    }
    
    public function register3()
    {
        $data = array(
            "mode" => $this->mode,
            "car" => "",
            "member" => ""
        );
        
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $data["car"] = $_POST["car"];
            unset($_POST["car"]);
            $data["member"] = json_encode($_POST);
        }
        
        $this->load->view("template/header");
        $this->load->view("pages/account/header_register");
        $this->load->view("pages/account/register3", $data);
        $this->load->view("template/footer");
    }
    
    public function register4()
    {
        $data = array(
            "mode" => $this->mode,
            "car" => "",
            "member" => "",
            "company" => ""
            
        );
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $data["car"] = $_POST["car"];
            unset($_POST["car"]);
            $data["member"] = $_POST["member"];
            unset($_POST["member"]);
            $data["company"] = json_encode($_POST);
            
            // INSERT DATA member, car & company
            $memberId = $this->member_model->register($data);
            $data["member_id"] = $memberId;
        }
        
        
        $this->load->view("template/header");
        $this->load->view("pages/account/header_register");
        $this->load->view("pages/account/register4", $data);
        $this->load->view("template/footer");
    }
    
    public function register5()
    {
        $data = array();
        
        // UPDATE member.photo & car.photo
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $memberId = $_POST["member_id"];
            $data["member_id"] = $memberId;
            $allowedExt = array('png','jpg');
            $maxFileSize = 1044070;
            
            $memberFileName = $_FILES['member_photo']['name'][0];
            if($memberFileName != ""){
                $memberX = explode('.', $memberFileName);
                $memberExt = strtolower(end($memberX));
                $memberFileName = $memberId . "." . $memberExt;
                $memberFileSize = $_FILES['member_photo']['size'][0];
                $memberFileTmp = $_FILES['member_photo']['tmp_name'][0];
                $memberDirectory = "assets/images/member/";

                if(in_array($memberExt, $allowedExt) === true){
                        if($memberFileSize < $maxFileSize){			
                                move_uploaded_file($memberFileTmp, $memberDirectory.$memberFileName);
                                $this->member_model->updateMemberPhoto($memberFileName,$memberId);
                        }else{
                                echo 'UKURAN FILE MEMBER TERLALU BESAR';
                        }
                }else{
                        echo 'EKSTENSI FILE MEMBER YANG DI UPLOAD TIDAK DI PERBOLEHKAN';
                }
            }
            
            $carFileName = $_FILES['car_photo']['name'][0];
            if($carFileName != ""){
                $carX = explode('.', $carFileName);
                $carExt = strtolower(end($carX));
                $carFileName = $memberId . "." . $carExt;
                $carFileSize = $_FILES['car_photo']['size'][0];
                $carFileTmp = $_FILES['car_photo']['tmp_name'][0];
                $carDirectory = "assets/images/car/";

                if(in_array($carExt, $allowedExt) === true){
                        if($carFileSize < $maxFileSize){			
                                move_uploaded_file($carFileTmp, $carDirectory.$carFileName);
                                $this->member_model->updateCarPhoto($carFileName,$memberId);
                        }else{
                                echo 'UKURAN FILE CAR TERLALU BESAR';
                        }
                }else{
                        echo 'EKSTENSI FILE CAR YANG DI UPLOAD TIDAK DI PERBOLEHKAN';
                }
            }
        }
        $this->load->view("template/header");
        $this->load->view("pages/account/header_register");
        $this->load->view("pages/account/register5", $data);
        $this->load->view("template/footer");
    }
    
    // registration succeed
    public function join()
    {
        $data = array();
        
        // UPDATE member.username & member.pin
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $memberId = $_POST["member_id"];
            $memberUsername = $_POST["username"];
            $memberPin = $_POST["password"];
            $this->member_model->updateMemberCredential($memberUsername, $memberPin,$memberId);
        }
        $this->load->view("template/header");
        $this->load->view("pages/account/join", $data);
        $this->load->view("template/footer");
    }
    
    public function signin()
    {
        $data = array(
            "response" => "",
            "username" => "",
            "password" => ""
        );

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $username = $_POST["username"];
            $pin = $_POST["password"];
            $memberData = $this->member_model->sign_in($username,$pin);
//            die(print_r($member));
            if(count($memberData) > 0){
                $memberId = 0;
                $memberNickName = "Guest";
                foreach($memberData as $member){
                    $memberId = $member["id"];
                    $memberNickName = $member["call_name"];
                }
                
                $this->session->set_userdata(SESS_MEMBER_ID, $memberId);
                $this->session->set_userdata(SESS_MEMBER_PIN, $pin);
                $this->session->set_userdata(SESS_MEMBER_NICKNAME, $memberNickName);
                
                redirect(base_url()."account/profile/".$memberId);
            }else{
                $data["response"] = "Username / Pin tidak sesuai";
                $data["username"] = $username;
                $data["password"] = $pin;
            }
        }
        
        $this->load->view("template/header");
        $this->load->view("pages/account/signin", $data);
        $this->load->view("template/footer");
    }
    
    public function profile($userId="")
    {
        $data = array();
        $memberData = $this->member_model->get_member_data($userId);
        foreach($memberData as $member){
            $data = $member;
        }
        
        $data["imagePath"] = base_url()."assets/images/member/";
        $data["userId"] = $userId;
        $data["defaultNull"] = "-";
//        die(print_r($data));
        
        $this->load->view("template/header");
        $this->load->view("pages/account/profile", $data);
//        $this->load->view("template/footer");
    }
    
    public function profile_edit($userId="")
    {
        $data = array();
        
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $memberData = $this->member_model->updateProfile($userId,$_POST);
        }
        
        $memberData = $this->member_model->get_member_data($userId);
        foreach($memberData as $member){
            $data = $member;
        }
        
        $data["userId"] = $userId;
        $data["imagePath"] = base_url()."assets/images/member/";
        
        $this->load->view("template/header");
        $this->load->view("pages/account/profile_edit", $data);
//        $this->load->view("template/footer");
    }
    
    public function profile_photo($userId="")
    {
        $data = array();
        
        $data["userId"] = $userId;
        // UPDATE member.photo & car.photo
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $memberId = $_POST["member_id"];
            $data["member_id"] = $memberId;
            $allowedExt = array('png','jpg');
            $maxFileSize = 1044070;
            
            $memberFileName = $_FILES['member_photo']['name'][0];
            if($memberFileName != ""){
                $memberX = explode('.', $memberFileName);
                $memberExt = strtolower(end($memberX));
                $memberFileName = $memberId . "." . $memberExt;
                $memberFileSize = $_FILES['member_photo']['size'][0];
                $memberFileTmp = $_FILES['member_photo']['tmp_name'][0];
                $memberDirectory = "assets/images/member/";

                if(in_array($memberExt, $allowedExt) === true){
                        if($memberFileSize < $maxFileSize){			
                                move_uploaded_file($memberFileTmp, $memberDirectory.$memberFileName);
                                $this->member_model->updateMemberPhoto($memberFileName,$memberId);
                        }else{
                                echo 'UKURAN FILE MEMBER TERLALU BESAR';
                        }
                }else{
                        echo 'EKSTENSI FILE MEMBER YANG DI UPLOAD TIDAK DI PERBOLEHKAN';
                }
            }
            
            $carFileName = $_FILES['car_photo']['name'][0];
            if($carFileName != ""){
                $carX = explode('.', $carFileName);
                $carExt = strtolower(end($carX));
                $carFileName = $memberId . "." . $carExt;
                $carFileSize = $_FILES['car_photo']['size'][0];
                $carFileTmp = $_FILES['car_photo']['tmp_name'][0];
                $carDirectory = "assets/images/car/";

                if(in_array($carExt, $allowedExt) === true){
                        if($carFileSize < $maxFileSize){			
                                move_uploaded_file($carFileTmp, $carDirectory.$carFileName);
                                $this->member_model->updateCarPhoto($carFileName,$memberId);
                        }else{
                                echo 'UKURAN FILE CAR TERLALU BESAR';
                        }
                }else{
                        echo 'EKSTENSI FILE CAR YANG DI UPLOAD TIDAK DI PERBOLEHKAN';
                }
            }
            redirect(base_url()."account/profile/".$memberId);
        }
        
        $this->load->view("template/header");
        $this->load->view("pages/account/profile_photo", $data);
//        $this->load->view("template/footer");
    }
    
    public function profile_pin($userId="")
    {
        $data = array();
        
        $memberData = $this->member_model->get_member_data($userId);
        foreach($memberData as $member){
            $data = $member;
        }
        
        $data["response"] = "";
        $data["old_password"] = "";
        $data["new_password"] = "";
        $data["confirm_password"] = "";
        $data["userId"] = $userId;
        $data["imagePath"] = base_url()."assets/images/member/";
        $data["pin_lama"] = $this->session->userdata(SESS_MEMBER_PIN);
        
        // UPDATE member.username & member.pin
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $memberId = $_POST["member_id"];
            $memberUsername = $_POST["username"];
            $memberOldPin = $_POST["old_password"];
            $memberNewPin = $_POST["new_password"];
            $memberConfirmPin = $_POST["confirm_password"];
            
            $isVerified = false;
            $data["response"] = "Username <strong>".$memberUsername."</strong> sudah digunakan oleh anggota lain";
            $userData = $this->member_model->isExistUsername($memberUsername,$memberId);
            if(count($userData) == 0){
                $data["username"] = $memberUsername;
                $data["response"] = "Password lama yang Anda ketikkan tidak sesuai";
                if($memberOldPin == $this->session->userdata(SESS_MEMBER_PIN)){
                    $data["old_password"] = $memberOldPin;
                    $data["response"] = "Isikan terlebih dahulu password baru Anda";
                    if($memberNewPin != "" || $memberConfirmPin != ""){
                        $data["new_password"] = $memberNewPin;
                        $data["confirm_password"] = $memberConfirmPin;
                        $data["response"] = "Password baru dan konfirmasi password baru yang Anda ketikkan tidak sesuai";
                        if($memberNewPin == $memberConfirmPin){
                            $this->session->set_userdata(SESS_MEMBER_PIN, $memberNewPin);
                            $isVerified = true;
                        }
                    }
                }
            }
            
            if($isVerified){
                $this->member_model->updateMemberCredential($memberUsername, $memberNewPin,$memberId);
                redirect(base_url()."account/profile/".$memberId);
            }
        }
        
        $this->load->view("template/header");
        $this->load->view("pages/account/profile_pin", $data);
//        $this->load->view("template/footer");
    }
    
    public function signout()
    {
        session_destroy();
        redirect(base_url());
    }
}
