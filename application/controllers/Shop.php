<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends CI_Controller {

    function __construct() {	
        parent::__construct();        
    }
    
    public function index()
    {
        $data = array();
        $this->load->view("template/header");
        $this->load->view("pages/shop", $data);
        $this->load->view("template/footer");
    }
}
