<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller {

    function __construct() {	
        parent::__construct();      
        $this->load->model("event_model");
    }
    
    public function touring()
    {
        $data = array(
            "title" => "Touring",
            "subtitle" => "Kegiatan Rutin Kami",
            "imagePath" => base_url()."assets/images/event/touring/",
            "eventList" => array()
        );
        $eventList = $this->event_model->get_event_list("touring");
        $data["eventList"] = $eventList;
        
        $this->load->view("template/header");
        $this->load->view("pages/event/event", $data);
        $this->load->view("template/footer");
    }
    
    public function meeting()
    {
        $data = array(
            "title" => "Kopi Darat",
            "subtitle" => "Kegiatan Rutin Kami",
            "imagePath" => base_url()."assets/images/event/meeting/",
            "eventList" => array()
        );
        $eventList = $this->event_model->get_event_list("meeting");
        $data["eventList"] = $eventList;
        
        $this->load->view("template/header");
        $this->load->view("pages/event/event", $data);
        $this->load->view("template/footer");
    }
    
    public function social()
    {
        $data = array(
            "title" => "Bakti Social",
            "subtitle" => "Kegiatan Rutin Kami",
            "imagePath" => base_url()."assets/images/event/social/",
            "eventList" => array()
        );
        $eventList = $this->event_model->get_event_list("social");
        $data["eventList"] = $eventList;
        $this->load->view("template/header");
        $this->load->view("pages/event/event", $data);
        $this->load->view("template/footer");
    }
    
    public function detail($eventId="")
    {
        $data = array(
            "title" => "Detail Event",
            "subtitle" => "Kegiatan Rutin Kami",
            "imagePath" => base_url()."assets/images/event/",
            "imagePathEvent" => base_url()."assets/images/event/",
            "eventData" => array()
        );
        $eventData = $this->event_model->get_event_data($eventId);
        $data["eventData"] = $eventData;
        $data["eventFacilitate"] = $this->event_model->get_event_facilitate($eventId);
        $data["eventMore"] = $this->event_model->get_event_more($eventId);
        
        foreach($eventData as $event){
            $data["eventName"] = $event["name"];
            $data["eventType"] = $event["type"];
            $data["eventTitle"] = $event["event_title"];
            $data["eventDescription"] = $event["event_description"];
            $data["eventParticipant"] = $event["participants"];
            $data["eventMeetPoint"] = $event["meeting_point"];
            $data["eventLocation"] = $event["location"];
            $data["eventDistance"] = $event["distance"];
            $data["eventDuration"] = $event["duration"];
            $data["eventDate"] = $event["date"];
            $data["imagePathEvent"] .= $event["type"] ."/";
            
        }
        
        $this->load->view("template/header");
        $this->load->view("pages/event/detail", $data);
        $this->load->view("template/footer");
    }
}
