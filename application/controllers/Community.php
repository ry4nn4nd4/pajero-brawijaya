<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Community extends CI_Controller {

    function __construct() {	
        parent::__construct();        
        $this->load->model("member_model");
    }
    
    public function index()
    {
        $data = array();
        $this->load->view("template/header");
        $this->load->view("pages/community", $data);
        $this->load->view("template/footer");
    }
    
    public function marshal()
    {
        $data = array(
            "title" => "Komunitas",
            "subtitle" => "Marshal",
            "imagePath" => base_url()."assets/images/member/",
            "chapter" => array(),
            "sector" => array()
        );
        $sector = $this->member_model->get_sector_data();
        $chapter = $this->member_model->get_marshal_list();
        
        foreach($chapter as $row){
            if($row["sector_id"] == 1){
                array_push($data["chapter"],$row);
            }
        }
        
        foreach($sector as $sectorData){
                $ketua = array();
                $anggota = array();
                foreach($chapter as $chapterData){
                    if($chapterData["sector_id"] == $sectorData["id"]){
                        if($chapterData["position"] == "Ketua"){
                            array_push($ketua, $chapterData);
                        }else{
                            array_push($anggota, $chapterData);
                        }
                    }
                }
                $marshal = array(
                    "name" => $sectorData["name"],
                    "ketua" => $ketua,
                    "anggota" => $anggota
                );
                array_push($data["sector"], $marshal);
        }

//        print_r($data["sector"]);
//        die();
        
        
        $this->load->view("template/header");
        $this->load->view("pages/community/marshal", $data);
        $this->load->view("template/footer");
    }
    
    public function member()
    {
        $data = array(
            "title" => "Komunitas",
            "subtitle" => "Anggota",
            "imagePath" => base_url()."assets/images/member/",
            "memberList" => array()
        );
        
        $memberList = $this->member_model->get_member_list();
        $data["memberList"] = $memberList;
        
        $this->load->view("template/header");
        $this->load->view("pages/community/member", $data);
        $this->load->view("template/footer");
    }
}
